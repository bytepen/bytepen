import * as React from 'react';
import logo from './Patreon.png';

export const PatreonLogo = () => {
    return (
        <img src={logo}/>
    );
};