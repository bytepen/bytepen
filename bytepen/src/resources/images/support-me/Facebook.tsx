import * as React from 'react';
import logo from './Facebook.png';

export const FacebookLogo = () => {
    return (
        <img src={logo}/>
    );
};