import * as React from 'react';
import logo from './Amazon.png';

export const AmazonLogo = () => {
    return (
        <img src={logo}/>
    );
};