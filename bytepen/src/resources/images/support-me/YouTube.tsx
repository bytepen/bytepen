import * as React from 'react';
import logo from './YouTube.png';

export const YouTubeLogo = () => {
    return (
        <img src={logo}/>
    );
};