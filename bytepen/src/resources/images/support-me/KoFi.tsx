import * as React from 'react';
import logo from './KoFi.png';

export const KoFiLogo = () => {
    return (
        <img src={logo}/>
    );
};