import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { App } from './App';
import { StyledAppBanner } from './component/banner/AppBanner';

describe('App', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<App/>);
    });

    it('should render a banner', () => {
        expect(subject.find(StyledAppBanner).exists()).toBeTruthy();
    });
});