import { Provider } from 'mobx-react';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { StyledApp } from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { actions } from './utils/Actions';
import { stores } from './utils/Stores';

ReactDOM.render(
    <Provider
        {...stores}
        {...actions}
    >
        <BrowserRouter>
            <StyledApp/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();
