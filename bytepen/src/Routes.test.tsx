import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { Route, Switch } from 'react-router';
import { Routes } from './Routes';

describe('Routes', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<Routes/>);
    });

    it('should support all routes', () => {
        const routes = [
            '/',
            '/zzblank',
            '/support-me',
            '/vlc',
            '/tutorials'
        ];

        for (let i = 0; i < routes.length; i++) {
            expect(subject.find(Route).at(i).prop('path')).toBe(routes[i]);
        }
    });
});