import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledYouTubeContainer } from '../component/youtube/YouTubeContainer';
import { HomePage } from './HomePage';

describe('HomePage', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<HomePage/>);
    });

    it('should contain a welcome message', () => {
        expect(subject.find('.body').text()).toContain('Welcome to BytePen Studios!');
    });

    it('should render a YouTubeContainer', () => {
        expect(subject.find(StyledYouTubeContainer).exists()).toBeTruthy();
    });
});