import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledSupportMeContainer } from '../component/supportMe/SupportMeContainer';
import { SupportMePage } from './SupportMePage';

describe('SupportMePage', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<SupportMePage/>);
    });

    it('should render a support me container', () => {
        expect(subject.find(StyledSupportMeContainer).exists()).toBeTruthy();
    });
});