import { YouTubeStore } from '../component/youtube/stores/YouTubeStore';

const youtubeStore = new YouTubeStore();

export interface Stores {
    youtubeStore: YouTubeStore;
}

export const stores = {
    youtubeStore
};