import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { TopMenuOption } from './TopMenuOption';

describe('MenuOption', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(
            <TopMenuOption
                title="Test Option"
                href="https://www.google.com"
            />
        );
    });

    it('should have a title', () => {
        expect(subject.find('.menuOption').text()).toBe('Test Option');
    });

    it('should have an id', () => {
        expect(subject.find('.menuOption').html()).toContain('"TestOption"');
    });

    it('should have a url', () => {
        expect(subject.find('a').at(0).props().href).toBe('https://www.google.com');
    });
});