import * as React from 'react';
import styled from 'styled-components';
import { StyledTopMenuOption } from './TopMenuOption';
import MenuOptions from '../menu.json';
import Playlists from '../../../json/Playlists.json';

interface Props {
    className?: string;
}

export class TopMenuContainer extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                {
                    MenuOptions.items.map((i: { title: string, href: string, children: [] }, index) => {
                        if (i.title === 'Tutorials') {
                            return (
                                <StyledTopMenuOption key={index} title={i.title} href={i.href}>
                                    {
                                        Playlists.items.filter((c: any) => {
                                            return c.snippet.description.substr(-1) === "!"
                                        }).sort((a, b) => (a.snippet.title > b.snippet.title) ? 1 : -1)
                                            .map((c: any, cindex: number) => {
                                                const currentTitle = c.snippet.title
                                                    .toLowerCase()
                                                    .replace(/\s+/g, '-')
                                                    .replace(/[^a-zA-Z0-9-_]/g, '');
                                                return (<StyledTopMenuOption key={cindex}
                                                                             title={c.snippet.title}
                                                                             href={currentTitle}
                                                                             className={"dd-option"}/>)
                                            })
                                    }
                                </StyledTopMenuOption>
                            )
                        }
                        if (i.children === null) {
                            return (
                                <StyledTopMenuOption key={index} title={i.title} href={i.href}/>
                            )
                        }
                        return (
                            <StyledTopMenuOption key={index} title={i.title} href={i.href}>
                                {
                                    i.children && i.children.map((c: { title: string, href: string, children: [] }, cindex) => {
                                        return (<StyledTopMenuOption key={cindex} title={c.title} href={c.href}/>)
                                    })
                                }
                            </StyledTopMenuOption>
                        )
                    })
                }
            </div>
        );
    }
}

export const StyledTopMenuContainer = styled(TopMenuContainer)`
position: absolute;
display: flex;

.theWilsonMinuteOption {
  width: 205px;
}

@media only screen and (max-width: 625px) {
  display: none;
}
`;