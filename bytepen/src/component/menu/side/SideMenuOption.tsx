import * as React from 'react';
import styled from 'styled-components';
import { mdiChevronDown } from '@mdi/js';
import Icon from '@mdi/react';

interface Props {
    className?: string;
    title: string;
    href: string;
    childOptions?: Array<{}>;
    depth?: number;
}

interface State {
    childrenHeight: string;
    depth: number;
}

export class SideMenuOption extends React.Component<Props, State> {
    constructor(props: any, state: any) {
        super(props, state);

        this.state = {
            childrenHeight: '0',
            depth: this.props.depth || 0
        };
    }

    public toggleOpen(e: any) {
        if (this.props.childOptions) {
            e.preventDefault();
            if (this.state.childrenHeight === '0') {
                this.setState({childrenHeight: (this.props.childOptions.length * 50) + "px"});
            } else {
                this.setState({childrenHeight: '0'});
            }
        }
    }

    public render() {
        return (
            <div
                className={this.props.className}
            >
                <a
                    href={this.props.href}
                    className={"menuOption level" + this.state.depth}
                    id={this.props.title}
                    onClick={(e) => {
                        this.toggleOpen(e)
                    }}
                >
                    {this.props.title}
                    {
                        this.props.childOptions &&
                        <Icon
                            path={mdiChevronDown}
                            className={"chevronDown"}
                            style={{transform: "scaleY(" + (this.state.childrenHeight === '0' ? 1 : -1) + ")"}}
                        />
                    }
                </a>
                {
                    this.props.childOptions &&
                    <div className={"menuChildren"} style={{height: this.state.childrenHeight}}>
                        {
                            this.props.childOptions.map((o: any, k: number) => {
                                return (<SideMenuOption key={k} title={o.title} href={o.href}
                                                        depth={this.state.depth + 1}/>)
                            })
                        }
                    </div>
                }
            </div>
        );
    }
}

export const StyledSideMenuOption = styled(SideMenuOption)`
line-height: 50px;
overflow: hidden;
white-space: nowrap;
width: 100%;

.menuOption {
  width: 100%;
  text-decoration: none;
  color: #AAA;
  font-size: 15px;
  padding-left: 15px;
  display: flex;
  align-items: center;
}

.chevronDown {
  position: absolute;
  right: 20px;
  width: 20px;
  transition: transform 200ms;
  fill: #AAA;
}

.menuChildren {
  background: #07333B;
  height: 0;
  transition: height 200ms;
}
`;