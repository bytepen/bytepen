import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { SideMenuOption } from './SideMenuOption';

describe('SideMenuOption', () => {
    let subject: ShallowWrapper;
    let toggleOpenSpy: jest.SpyInstance;
    beforeEach(() => {
        subject = shallow(
            <SideMenuOption
                title="test"
                href="https://www.google.com"
                childOptions={[{title: "sub", href: "https://www.yahoo.com"}]}
            />
        );

        toggleOpenSpy = jest.spyOn(subject.instance() as SideMenuOption, "toggleOpen");
    });

    it('should have a title', () => {
        expect(subject.find('.menuOption').text()).toContain('test');
    });

    it('should have a url', () => {
        expect(subject.find('a').at(0).props().href).toBe('https://www.google.com');
    });

    it('should be able to hold more of itself', () => {
        expect(subject.find('.menuChildren').prop('children')![0].props.href).toBe('https://www.yahoo.com');
    });

    it('should have a dropdown icon and expand when clicked when it has children', () => {
        expect(subject.find('.menuChildren').prop('style')).toHaveProperty('height', '0');
        expect(subject.find(".chevronDown").exists()).toBeTruthy();
        subject.find('a').simulate('click', {
            preventDefault: () => {
                return
            }
        });
        expect(toggleOpenSpy).toHaveBeenCalled();
        expect(subject.find('.menuChildren').prop('style')).toHaveProperty('height', '50px');
    });

    it('should not have a dropdown icon and link to a url when it does not have children', () => {
        subject = shallow(
            <SideMenuOption
                title="test"
                href="https://www.google.com"
            />
        );
        expect(subject.find(".dropdownIcon").exists()).toBeFalsy();
    });

});
