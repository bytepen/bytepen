import { action } from 'mobx';
import * as React from 'react';
import styled from 'styled-components';
import { StyledSideMenuOption } from './SideMenuOption';
import MenuOptions from '../menu.json';
import Playlists from '../../../json/Playlists.json';

interface Props {
    className?: string;
}

interface State {
    display: string;
    width: string;
    startSwipeX: number;
    currentSwipeX: number;
}

export class SideMenuContainer extends React.Component<Props, State> {
    constructor(props: any, state: any) {
        super(props, state);

        this.state = {
            display: 'none',
            width: '0',
            startSwipeX: -1,
            currentSwipeX: -1,
        };
    }

    @action.bound
    public mobileMenuIconClick() {
        this.setState({
            display: 'block',
            startSwipeX: -1,
            currentSwipeX: -1
        });
        document.getElementsByTagName('body')[0].style.overflow = "hidden";
        setTimeout(() => {
            this.setState({
                width: "65%"
            })
        }, 50)
    }

    @action.bound
    public closeSideMenuClick() {
        this.setState({
            width: '0',
            startSwipeX: -1,
            currentSwipeX: -1
        });
        document.getElementsByTagName('body')[0].style.overflow = "unset";

        setTimeout(() => {
            this.setState({display: "none"});
        }, 200)
    }

    @action.bound
    public handleSwipe() {
        if ((this.state.startSwipeX === this.state.currentSwipeX || this.state.currentSwipeX === -1) && this.state.startSwipeX > document.body.clientWidth * 0.65) {
            this.closeSideMenuClick();
        }
        if (this.state.currentSwipeX < this.state.startSwipeX - 50 && this.state.currentSwipeX >= 0 && this.state.startSwipeX >= 0) {
            this.closeSideMenuClick();
        }
    }

    public render() {
        return (
            <div
                className={this.props.className}
            >
                <div
                    className="mobileMenuIcon"
                    onClick={this.mobileMenuIconClick}
                >
                    <div className="mobileMenuIconBar1"/>
                    <div className="mobileMenuIconBar2"/>
                    <div className="mobileMenuIconBar3"/>
                </div>
                <div
                    className={"menuHolder"}
                    style={{
                        backgroundColor: "rgba(0,0,0," + (parseFloat(this.state.width) * 0.01) + ")",
                        display: this.state.display
                    }}
                    onTouchStart={(event) => {
                        this.setState({startSwipeX: event.targetTouches[0].clientX})
                    }}
                    onTouchMove={(event) => {
                        this.setState({currentSwipeX: event.targetTouches[0].clientX})
                    }}
                    onTouchEnd={this.handleSwipe}
                    onMouseDown={(e) => {
                        this.setState({startSwipeX: e.clientX})
                    }}
                    onMouseMove={(e) => {
                        this.setState({currentSwipeX: e.clientX})
                    }}
                    onMouseUp={this.handleSwipe}
                >
                    <div
                        className="sideMenu"
                        id="sideMenu"
                        style={{"width": this.state.width}}
                    >
                        {
                            MenuOptions.items.map((i: { title: string, href: string, children: [] }, index) => {
                                if (i.title === 'Tutorials') {
                                    const twmChildren = Playlists.items.filter((c: any) => {
                                        return c.snippet.description.substr(-1) === "!"
                                    }).sort((a, b) => (a.snippet.title > b.snippet.title) ? 1 : -1)
                                        .map((c: any) => {
                                            const currentTitle = c.snippet.title
                                                .toLowerCase()
                                                .replace(/\s+/g, '-')
                                                .replace(/[^a-zA-Z0-9-_]/g, '');
                                            return ({title: c.snippet.title, href: currentTitle})
                                        });

                                    twmChildren.push({title: "All Playlists", href: "/tutorials"});

                                    return (
                                        <StyledSideMenuOption key={index} title={i.title} href={i.href}
                                                              childOptions={twmChildren}/>
                                    )
                                }
                                if (i.children === null) {
                                    return (
                                        <StyledSideMenuOption key={index} title={i.title} href={i.href}/>
                                    )
                                }
                                return (
                                    <StyledSideMenuOption key={index} title={i.title} href={i.href}
                                                          childOptions={i.children}/>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export const StyledSideMenuContainer = styled(SideMenuContainer)`
.sideMenu {
  background: #05272D;
  height: 100vh;
  transition: width 200ms;
  position: absolute;
  width: 0;
  -webkit-tap-highlight-color: transparent;
}

.sideMenu>div:nth-of-type(1) {
  margin-top: 13px;
}

.menuHolder{
  cursor: pointer;
  position: fixed;
  z-index: 1000;
  height: 100vh;
  width: 100%;
  background: rgba(0,0,0,0.5);
  user-select: none;
  -webkit-tap-highlight-color: transparent;
  transition: background-color 200ms;
}

.closeSideMenu {
  color: #FFFFFF;
  position: absolute;
  right: 0;
  padding: 10px 20px 20px 20px;
  line-height: 30px;
  font-size: 30px;
  text-align: end;
  cursor: pointer;
  overflow: hidden;
  z-index: 1010;
}

.mobileMenuIcon {
  background: orange;
  width: 35px;
  height: 25px;
  z-index: 100;
  top: 23px;
  left: 10px;
  position: absolute;
  display: none;
  
  .mobileMenuIconBar1 {
    background: white;
    width: 20px;
    height: 3px;
    margin-top: 5px;
    margin-left: 7px;
  }
    
  .mobileMenuIconBar2 {
    background: white;
    width: 20px;
    height: 3px;
    margin-top: 3px;
    margin-left: 7px;
  }
    
  .mobileMenuIconBar3 {
    background: white;
    width: 20px;
    height: 3px;
    margin-top: 3px;
    margin-left: 7px;
  }
}

@media only screen and (max-width: 625px) {
  .mobileMenuIcon {
    display: inline;
  }
}
`;