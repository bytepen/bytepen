import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { SideMenuContainer } from './SideMenuContainer';
import { StyledSideMenuOption } from './SideMenuOption';

describe('SideMenuContainer', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<SideMenuContainer/>);
    });
    
    it('should contain a menu with option', () => {
        expect(subject.find('.sideMenu').find(StyledSideMenuOption).exists()).toBeTruthy();
    });

    it('should contain a mobile menu icon', () => {
        expect(subject.find('.mobileMenuIcon').exists()).toBeTruthy();
    });
});