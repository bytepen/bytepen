import * as React from 'react';
import logo from './BytePenLogo_30x30.png';

export const BytePenLogo = () => {
    return (
        <img src={logo}/>
    );
};