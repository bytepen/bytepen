import * as React from 'react';
import styled from 'styled-components';

interface Props {
    className?: string;
}

export class AppFooter extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <p className={"disclaimer"}>BytePen Studios, LLC is not responsible for any damage you do to your property while following tutorials.</p>
            </div>
        );
    }
}

export const StyledAppFooter = styled(AppFooter)`
width: 100%;
background: #05272D;
margin-top: auto;
text-align: center;

.disclaimer {
    color: #AAAAAA;
}
`;