import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { AppFooter } from './AppFooter';

describe('AppFooter', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<AppFooter/>);
    });

    it('should display a disclaimer message', () => {
        expect(subject.find('.disclaimer').html()).toContain("BytePen Studios, LLC is not responsible for any damage you do to your property while following tutorials.")
    });
});