import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { VideoLengthModule } from './VideoLengthModule';

describe('VideoLengthModule', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(
            <VideoLengthModule
                title="Intro"
                maxSeconds={10}
            />
        );
    });

    it('should contain a title, textarea, and second counter', () => {
        expect(subject.find('.title').text()).toBe('Intro');
        expect(subject.find('textarea').exists()).toBeTruthy();
        expect(subject.find('.time').text()).toBe('0 Seconds');
    });

    it('should update the number of seconds when text is put in the textarea', () => {
        subject.find('textarea').simulate('change', { target: { value: 'This is a small amount of text.'}});
        expect(subject.find('.time').text()).toBe('1.55 Seconds');
    });
});