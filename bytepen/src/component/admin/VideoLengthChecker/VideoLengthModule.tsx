import { action } from 'mobx';
import * as React from 'react';
import styled from 'styled-components';

interface Props {
    className?: string;
    title: string;
    maxSeconds: number;
}

interface State {
    seconds: number;
    timeColor: string;
}

export class VideoLengthModule extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props, state);
        this.state={seconds: 0, timeColor: 'red'};
    }


    @action.bound
    public async evaluateText(e: any) {
        await this.setState({seconds: e.target.value.length / 20});

        if (this.state.seconds === 0 || this.state.seconds >= (this.props.maxSeconds*.9)) {
            await this.setState({timeColor: 'red'});
        } else {
            await this.setState({timeColor: 'green'});
        }
    }

    public render() {
        return (
            <div
                className={this.props.className + " " + this.props.title.toLowerCase() + "Section"}
            >
                <div className="title">{this.props.title}</div>
                <textarea rows={this.props.maxSeconds/5} onChange={this.evaluateText}/>
                <div className="time" style={{"color": this.state.timeColor}}>{this.state.seconds} Seconds</div>
            </div>
        );
    }
}

export const StyledVideoLengthModule = styled(VideoLengthModule)`
margin-bottom: 20px;

.title {
  font-weight: bold;
}

textarea {
width: 850px;
}
`;