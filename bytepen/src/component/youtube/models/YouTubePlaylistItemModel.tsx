import { observable } from 'mobx';
import { YouTubeSnippetModel } from './YouTubeSnippetModel';
import { ResourceModel } from './ResourceModel';

export class YouTubePlaylistItemModel {
    @observable public _snippet: YouTubeSnippetModel;
    @observable public _position: number = 0;
    @observable public _resourceID: ResourceModel;
}