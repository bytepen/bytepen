import { action, computed, observable } from 'mobx';
import { YouTubeSlideModel } from './YouTubeSlideModel';

export class YouTubeArticleModel {
    @observable private _slides: YouTubeSlideModel[] = [];

    constructor(slides: YouTubeSlideModel[] = []) {
        this._slides = slides;
    }

    @computed
    get slides(): YouTubeSlideModel[] {
        return this._slides;
    }

    @action.bound
    public setSlides(slides: YouTubeSlideModel[]) {
        this._slides = slides;
    }
}