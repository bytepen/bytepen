import { inject, observer } from 'mobx-react';
import * as React from 'react';
import styled from 'styled-components';
import { YouTubeActions } from './actions/YouTubeActions';
import { StyledYouTubeArticle } from './article/YouTubeArticle';
import { StyledYouTubeList } from './list/YouTubeList';
import { YouTubeStore } from './stores/YouTubeStore';
import { StyledYouTubeVideo } from './video/YouTubeVideo';

interface Props {
    className?: string;
    playlist?: string;
    youtubeStore?: YouTubeStore;
    youtubeActions?: YouTubeActions;
    search?: string;
}

@observer
export class YouTubeContainer extends React.Component<Props> {
    public async componentWillMount() {
        if (this.props.playlist) {
            await this.props.youtubeActions!.setupPlaylist(this.props.playlist, this.props.search);
        } else {
            await this.props.youtubeActions!.setupLatestVideo();
        }
    }

    public listItemClicked = async (e: any) => {
        const videoID = e.target.id;
        await this.props.youtubeActions!.setupCurrentVideo(videoID);

        if (document.getElementsByClassName('selectedListItem').length > 0) {
            document.getElementsByClassName('selectedListItem')[0].classList.remove('selectedListItem');
        }
        if (document.getElementById(videoID)) {
            document.getElementById(videoID)!.classList.add('selectedListItem');
        }
    };

    public render() {
        return (
            <div
                className={this.props.className}
            >
                {
                    this.props.youtubeStore!.playlist &&
                    this.props.youtubeStore!.playlist.videos.length > 0 &&
                    <StyledYouTubeList
                        videos={this.props.youtubeStore!.playlist.videos}
                        onClick={this.listItemClicked}
                        fullTitles={this.props.youtubeStore!.fullTitles}
                        className={"youtubeList"}
                    />
                }
                <div className={"currentVideo"}>
                    <StyledYouTubeVideo
                        videoId={this.props.youtubeStore!.currentVideo.id}
                    />
                    <StyledYouTubeArticle
                        article={this.props.youtubeStore!.article}
                    />
                </div>
            </div>
        );
    }
}

export const StyledYouTubeContainer = inject('youtubeStore', 'youtubeActions')(styled(YouTubeContainer)`
@media (min-width: 1080px) {
  display: flex;
  width: 100%;
  justify-content: center;
  
  .youtubeList {
    max-width: 250px;
    min-width: 250px;
    border-bottom: none;
    border-right: 1px solid rgba(0,0,0,0.25);
    margin-right: 15px;
  }
}
`);