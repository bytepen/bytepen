import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';

export interface YouTubeRepository {
    findAllVideosInPlaylist(id: string): Promise<YouTubeVideoModel[]>;
    findPlaylistIdByTitle(title: string): Promise<string>;
    findLatestVideo(): Promise<YouTubeVideoModel>;
    findArticleByVideoID(id: string): YouTubeArticleModel;
}