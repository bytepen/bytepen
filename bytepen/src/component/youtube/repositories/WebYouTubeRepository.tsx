import * as Showdown from 'showdown';
import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';
import { YouTubeRepository } from './YouTubeRepository';
import { YouTubeSlideModel } from '../models/YouTubeSlideModel';
import '../../../resources/extensions/showdown/prettify';

export class WebYouTubeRepository implements YouTubeRepository {
    public findAllVideosInPlaylist(id: string) {
        const playlist = require('../../../json/playlists/' + id + '.json');

        return playlist.items.map((v: any) => {
            return new YouTubeVideoModel(v.snippet.resourceId.videoId, v.snippet.title);
        });
    }

    public findPlaylistIdByTitle(title: string) {
        const playlists = require('../../../json/Playlists.json');

        return playlists.items.filter((p: any) => p.snippet.title === title).map((p: any) => {
            return p.id;
        })[0];
    }

    public findLatestVideo() {
        const latest = require('../../../json/LatestVideo.json');
        return latest.items.map((v: any) => new YouTubeVideoModel(v.id.videoId, v.snippet.title))[0];
    }

    public findArticleByVideoID(id: string): YouTubeArticleModel {
        require("../../../resources/extensions/showdown/prettify");
        const showdown = new Showdown.Converter({
            literalMidWordUnderscores: true,
            extensions: ['prettify']
        });
        let slides: YouTubeSlideModel[] = [];
        try {
            const article = require('../../../json/articles/' + id + '.json');
            slides = article.slides.map((a: any) => {
                const html = a.content.map((s: string) => {
                    return showdown.makeHtml(s);
                });

                return new YouTubeSlideModel(a.images, html);
            });
        } catch (e) {
            slides = []
        }
        return new YouTubeArticleModel(slides)
    }
}