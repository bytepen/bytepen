import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { YouTubeArticle } from './YouTubeArticle';
import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import { YouTubeSlideModel } from '../models/YouTubeSlideModel';

describe('YouTubeArticle', () => {
    let subject: ShallowWrapper;
    let article: YouTubeArticleModel

    beforeEach(() => {
        article = new YouTubeArticleModel(
            [
                new YouTubeSlideModel(["https://i.imgur.com/S2YMNhI.jpg", "https://i.imgur.com/l2vjKVa.jpg"], ['Here are some funny pictures']),
                new YouTubeSlideModel(["https://i.imgur.com/cdkydjg.jpg"], ['Here is a cat picture']),
                new YouTubeSlideModel(["https://i.imgur.com/5WdXxfJ.jpg"], ["Here is a pupper"])
            ]
        );

        subject = shallow(
            <YouTubeArticle
                article={article}
            />
        );
    });

    it('should render divs for swiper, slides, navigation, and pagination', () => {
        expect(subject.find('.swiper-container').exists()).toBeTruthy();
        expect(subject.find('.swiper-container').find('.swiper-wrapper').exists()).toBeTruthy();
        expect(subject.find('.swiper-container').find('.swiper-button-next').exists()).toBeTruthy();
        expect(subject.find('.swiper-container').find('.swiper-button-prev').exists()).toBeTruthy();
    });

    it('should render a coming soon message', () => {
        subject = shallow(
            <YouTubeArticle article={new YouTubeArticleModel([])}/>
        );
        expect(subject.html()).toContain('>This article is coming soon!</p>');
    });

});