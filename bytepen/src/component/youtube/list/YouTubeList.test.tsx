import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';
import { YouTubeList } from './YouTubeList';
import Mock = jest.Mock;

describe('YouTubeList', () => {
    let subject: ShallowWrapper;
    let onClickFunction: Mock;

    beforeEach(() => {
        onClickFunction = jest.fn();

        subject = shallow(
            <YouTubeList
                videos={[
                    new YouTubeVideoModel("abcdef", "Series: Video One"),
                    new YouTubeVideoModel("fedcba", "Series: Video Two")
                ]}
                onClick={onClickFunction}
                fullTitles={false}
            />
        );
    });

    it('should render a title for the list', () => {
        expect(subject.find('.listTitle').text()).toBe('Select a Video:');
    });

    it('should render a list of video links', () => {
        expect(subject.find('.listItem').length).toBe(2);
        expect(subject.find('.listItem').at(0).html()).toContain('>1: Video One<');
        expect(subject.find('.listItem').at(1).html()).toContain('>2: Video Two<');
        expect(subject.find('.listItem').at(0).html()).toContain('id="abcdef"');
        expect(subject.find('.listItem').at(1).html()).toContain('id="fedcba"');
    });

    it('should fire a passed function on click', () => {
        subject.find('.listItem').at(0).simulate('click');
        expect(onClickFunction).toHaveBeenCalled();
    });
});