import { YouTubeVideoModel } from '../models/YouTubeVideoModel';
import { StubYouTubeRepository } from '../repositories/StubYouTubeRepository';
import { YouTubeRepository } from '../repositories/YouTubeRepository';
import { YouTubeActions } from './YouTubeActions';

describe('YouTubeActions', () => {
    let subject: YouTubeActions;
    let youtubeStore: any;
    let youtubeRepository: YouTubeRepository;
    let findLatestVideoSpy: jest.SpyInstance;

    beforeEach(() => {
        youtubeRepository = new StubYouTubeRepository();
        findLatestVideoSpy = jest.spyOn(youtubeRepository, "findLatestVideo");

        youtubeStore = {
            setPlaylist: jest.fn(),
            setCurrentVideo: jest.fn(),
            setArticle: jest.fn(),
            playlist: {
                setId: jest.fn(),
                setTitle: jest.fn(),
                setVideos: jest.fn(),
                title: 'Playlist Title',
                videos: [
                    new YouTubeVideoModel('abc1', 'Test Video 1'),
                    new YouTubeVideoModel('abc2', 'Test Video 2'),
                    new YouTubeVideoModel('abc3', 'Test Video 3'),
                ],
            },
            currentVideo: new YouTubeVideoModel('abc1', 'Test Video 1')
        };

        subject = new YouTubeActions({youtubeStore} as any, {youtubeRepository} as any);
    });

    it('should set up a playlist given a playlist name', async () => {
        await subject.setupPlaylist('My Awesome Playlist');
        expect(youtubeStore.playlist.setTitle).toHaveBeenCalledWith('My Awesome Playlist');
        expect(youtubeStore.playlist.setId).toHaveBeenCalledWith('12345');
        expect(youtubeStore.playlist.setVideos).toHaveBeenCalledWith([
            new YouTubeVideoModel('abc1', 'Test Video 1'),
            new YouTubeVideoModel('abc2', 'Test Video 2'),
            new YouTubeVideoModel('abc3', 'Test Video 3'),
        ]);
        expect(youtubeStore.setCurrentVideo).toHaveBeenCalled();
        expect(youtubeStore.setArticle).toHaveBeenCalled();
    });

    it('should set up the latest video', async () => {
        await subject.setupLatestVideo();
        expect(findLatestVideoSpy).toHaveBeenCalled();
        expect(youtubeStore.setCurrentVideo).toHaveBeenCalled();
    });

    it('should set up the current video by title', async () => {
        await subject.setupCurrentVideo('Test Video 1');
        expect(youtubeStore.setCurrentVideo).toHaveBeenCalled();
        expect(youtubeStore.setArticle).toHaveBeenCalled();
    });
});