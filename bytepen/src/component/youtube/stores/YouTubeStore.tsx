import { action, computed, observable } from 'mobx';
import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import { YouTubePlaylistModel } from '../models/YouTubePlaylistModel';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';

export class YouTubeStore {
    @observable private _playlist: YouTubePlaylistModel = new YouTubePlaylistModel();
    @observable private _currentVideo: YouTubeVideoModel = new YouTubeVideoModel('', '');
    @observable private _article: YouTubeArticleModel = new YouTubeArticleModel([]);

    @action.bound
    public setCurrentVideo(video: YouTubeVideoModel) {
        this._currentVideo = video;
    }

    @action.bound
    public setPlaylist(playlist: YouTubePlaylistModel) {
        this._playlist = playlist;
    }

    @action.bound
    public setArticle(article: YouTubeArticleModel) {
        this._article = article;
    }

    @computed
    public get playlist() {
        return this._playlist;
    }

    @computed
    public get currentVideo() {
        return this._currentVideo;
    }

    @computed
    public get article() {
        return this._article;
    }

    @computed public get fullTitles() {
        const fullTitlePlaylists = [
            "Projects",
            "One-Offs"
        ];
        return (fullTitlePlaylists.indexOf(this._playlist.title) >= 0)
    }
}