import { observer } from 'mobx-react';
import * as React from 'react';
import ReactDOMServer from 'react-dom/server';
import styled from 'styled-components';

interface Props {
    className?: string;
    videoId: string;
}

@observer
export class YouTubeVideo extends React.Component<Props> {
    public state = {
        width: 400,
        height: 400 * 0.5625
    };

    public componentWillReceiveProps() {
        const html = ReactDOMServer.renderToString(
            <VideoContainer
                className="video"
                data={'https://www.youtube.com/embed/' + this.props.videoId}
                width="100%"
                height="100%"
            />
        );
        document.getElementsByClassName(this.props.className!)[0].innerHTML = html;
    }

    public componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions.bind(this));
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions.bind(this));
    }

    public render() {
        return (
            <div
                className={this.props.className}
                style={{"height": this.state.height, "width": this.state.width}}
            >
                <VideoContainer
                    className="video"
                    data={'https://www.youtube.com/embed/' + this.props.videoId}
                    width={this.state.width}
                    height={this.state.height}
                />
            </div>
        );
    }

    private updateDimensions() {
        let newWidth = document.getElementsByClassName('body')[0] ?
        document.getElementsByClassName('body')[0].clientWidth :
        400;
        newWidth = newWidth > 800 ? 800 : newWidth;
        const newHeight = newWidth * 0.5625;
        this.setState({width: newWidth, height: newHeight});
    }

}

export const StyledYouTubeVideo = styled(YouTubeVideo)``;

const VideoContainer = styled.object`
width: ${(props) => props.width}px;
max-width: 800px;
height: ${(props) => props.height}px;
max-height: 450px;
border: 1px solid rgba(0,0,0,0.1);
`;