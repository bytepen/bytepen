import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { YouTubeVideo } from './YouTubeVideo';

describe('YouTubeVideo', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(
            <YouTubeVideo
                videoId="9IX7sL8uhEE"
            />
        );
    });

    it('should render a youtube video', () => {
        expect(subject.find('.video').html()).toContain('9IX7sL8uhEE');
    });
});