import * as React from 'react';
import styled from 'styled-components';
import { AmazonLogo } from '../../resources/images/support-me/Amazon';
import { FacebookLogo } from '../../resources/images/support-me/Facebook';
import { InstagramLogo } from '../../resources/images/support-me/Instagram';
import { KoFiLogo } from '../../resources/images/support-me/KoFi';
import { PatreonLogo } from '../../resources/images/support-me/Patreon';
import { TwitterLogo } from '../../resources/images/support-me/Twitter';
import { YouTubeLogo } from '../../resources/images/support-me/YouTube';

interface Props {
    className?: string;
    image: any;
    title: string;
    description: string;
    href: string;
}

export class SupportMeModule extends React.Component<Props> {
    public render() {
        return (
            <a
                href={this.props.href}
                className={this.props.className + " module"}
            >
                <table>
                    <tbody>
                    <tr><td>{this.getImage(this.props.image)}<br/><br/></td></tr>
                    <tr><td>{this.props.title}<br/><br/></td></tr>
                    <tr><td>{this.props.description}</td></tr>
                    </tbody>
                </table>
            </a>
        );
    }

    private getImage(title: string): any {
        switch(title) {
            case "Amazon":
                return (<AmazonLogo/>);
            case "Facebook":
                return (<FacebookLogo/>);
            case "Instagram":
                return (<InstagramLogo/>);
            case "KoFi":
                return (<KoFiLogo/>);
            case "Patreon":
                return (<PatreonLogo/>);
            case "Twitter":
                return (<TwitterLogo/>);
            case "YouTube":
                return (<YouTubeLogo/>);
        }
    }
}

export const StyledSupportMeModule = styled(SupportMeModule)`
margin-bottom: 40px;

table {
  width: 200px;
  height: 225px;
  display: inline-block;
  text-align: center;
}
`;