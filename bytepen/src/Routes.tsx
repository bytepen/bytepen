import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { StyledHomePage } from './page/HomePage';
import { StyledPlaylistPage } from './page/PlaylistPage';
import { StyledSupportMePage } from './page/SupportMePage';
import { StyledVideoLengthCheckerPage } from './page/VideoLengthCheckerPage';
import * as Playlists from './json/Playlists.json';
import * as VideoURLs from './json/urls.json';
import { StyledTutorialsPage } from './page/TutorialsPage';

export class Routes extends React.Component {


    public render() {
        return (
            <Switch>
                <Route exact={true} path="/" render={() => <StyledHomePage/>}/>
                <Route
                    exact={true}
                    path="/zzblank"
                    component={() => {
                        window.location.href = "https://drive.google.com/open?id=1JlykyxDPejxrE8RnsZDv7SnwlGQ_d9-w";
                        return null;
                    }}
                />
                <Route exact={true} path="/support-me" render={() => <StyledSupportMePage/>}/>
                <Route
                    exact={true}
                    path="/vlc"
                    render={() =>
                        <StyledVideoLengthCheckerPage/>
                    }
                />
                <Route
                    exact={true}
                    path="/tutorials"
                    render={() =>
                        <StyledTutorialsPage/>
                    }
                />
                {
                    Playlists.items.map((p: any, i: number) => {
                        return (
                            <Route
                                key={i}
                                path={
                                    "/" +
                                    p.snippet.title.toLowerCase().replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-_]/g, '')
                                }
                                render={(props) =>
                                    <StyledPlaylistPage playlist={p.snippet.title} {...props}/>
                                }
                            />
                        );
                    })
                }
                {
                    Object.keys(VideoURLs).map((id: string, i: number) => {
                        return (
                            <Route
                                key={i}
                                path={"/" + id}
                                render={() =>
                                    <div>
                                        <Redirect to={VideoURLs[id]}/>
                                        <p>
                                            Redirecting...
                                            <br/><br/>
                                            Click the following link if still waiting after 5 seconds:
                                            <br/>
                                            <a href={VideoURLs[id]}>Redirect</a>
                                        </p>
                                    </div>
                                }
                            />
                        )
                    })
                }
            </Switch>
        );
    }
}