The Legend of VIctor is a game that I created for the Shenanijam 2019 game jam led by the game development company
Butterscotch Shenanigans. I have been wanting to create a game that teaches the player a little about the VI text
editor ever since I saw the game [VIM Adventures](https://vim-adventures.com) and saw this as a good time to play around
with this idea.

You can check out the video I created for it above and you can actually play it in your web browser, Windows, or Linux
by visiting the following itch.io page:

[The Legend of VIctor (Shenanijam 2019)](https://bytepen.itch.io/shenanijam-2019)

----------
VideoID:gIJo9rGt7Dk