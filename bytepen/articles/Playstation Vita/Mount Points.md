We will be taking a look at the various mount points on the Playstation Vita. Nearly all of these mount points should
not be modified unless you know what you are doing or instructed otherwise.

**os0: CoreOS Files**

Files in this mount point are used for the core functionality of the Vita operating system.

**pd0: Welcome Park App**

Files in this mount point are used for the Welcome Park App and the Welcome Video.

**sa0: Fonts & Dictionaries**

Files in this mount point are bulkier items that the rest of the Vita depends on. Some examples are Fonts and
Dictionaries.

**tm0: NpDRM/DevKit**

Files in this mount point relate to NpDRM and DevKit. Even if you are trying to get around DRM, you should not be
messing with this mount point.

**ud0: System Updates**

Files in this mount point are used for System Updates. When you update your Vita, a PUP file is saved onto the mount
point and your Vita is rebooted. During the reboot, the Vita reads the PUP from this mount point to update files in the
other mount points.

**uma0: USB Port**

On the Playstation TV, this is the mount point for the USB port. There is no USB port on the Vita, so this mount point
isn't used. That makes this a pretty good mount point to use with SD2VITA.

**ur0: User Data (LiveArea Layout)**

This mount point is where User Data, such as the LiveArea icon Layout, is stored. The ur0:tai/ folder is a great place
to keep stable plugins as they are not dependent on your memory card or SD2VITA.

**ux0: Sony Memory Card**

This is the default mount point for the official Sony memory card. It is the only mount point you can store applications
that should be loaded on the LiveArea. If you are using SD2VITA and your microSD card is larger than your official
memory card, you should strongly consider mounting the SD2VITA here.

**vd0: Settings**

Files in this mount point pertain to the Settings on your Vita set via the Settings app. This is similar to a Registry
on other systems.

**vs0: Default Apps**

Files in this mount point are used for the default applications that are installed on your Vita, such as the Camera.

**imc0: Vita 2000 Storage**

This is the default mount point for the 1GB of storage built into the Playstation Vita 2000 Model.

**gro0: Game (Read-Only)**

This is the default mount point for the read-only portion of a game cartridge.

**grw0: Game (Read-Write)**

This is the default mount point for the read-write portion of a game cartridge. Things such as game saves would
typically be stored here. This is a good mount point to use with the SD2VITA as the SD2VITA consumes your game card
slot.
----------
VideoID:aWQEA32cb10