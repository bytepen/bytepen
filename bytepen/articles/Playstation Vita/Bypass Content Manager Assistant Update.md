When you are trying to use Sony's Content Manager Assistant and you are not on the latest firmware, you may see the
following message:

`You must update the system software of this system`

Thankfully there is a way to get around this message and it isn't too hard.

-----

### Uninstall Sony's Content Manager Assistant

The first thing you will need to do is to uninstall Sony's Content Manager Assistant. This process is the same as any
other program installed on your computer, but varies depending on your Operating System.

-----

### Install QCMA

Now that Sony's Content Manager Assistant has been uninstalled from your system, you will need to install QCMA.

It can be downloaded from the following link and installed like any other program if you are on Windows or macOS.

[QCMA GitHub (Windows/macOS)](https://github.com/codestation/qcma/releases)

If you are on Linux, you will need to run the following commands in terminal:

#### Debian (Ubuntu)

```shell
wget -O qcma.deb https://download.opensuse.org/repositories/home:/codestation/xUbuntu_18.04/amd64/qcma_0.4.2_amd64.deb
sudo dpkg -i qcma.deb
```

#### Fedora

```shell
wget -O qcma-x86_64.rpm https://download.opensuse.org/repositories/home:/codestation/Fedora_29/x86_64/qcma-0.4.2-4.1.x86_64.rpm
rpm -i qcma-x86_64.rpm
```

#### openSUSE

```shell
wget -O qcma-x86_64.rpm https://download.opensuse.org/repositories/home:/codestation/openSUSE_Tumbleweed/x86_64/qcma-0.4.2-4.85.x86_64.rpm
rpm -i qcma-x86_64.rpm
```

-----

### Run & Configure QCMA

After QCMA has been installed, you should be able to run it and see it in your task bar. Right click it and click
`Settings`.

Go to the `Other` tab and ensure the following settings are set:

Offline Mode: `Checked`  
Use this version for updates: `FW 0.00 (Always up-to-date)`

-----

### Confirm Fixed

Open `Content Manager` on your Vita and check that the update prompt is no longer there. If it is, you will need to take
the following steps. Note that this will reset the placement of icons on your home screen, but nothing serious should be
affected.

1. Hold the `Power Button` for 2 seconds and click `Power Off`  
2. Hold down the `Power Button`, `PlayStation Button`, and `R Trigger` for 5 seconds to boot into `Safe Mode`  
3. Select `Rebuild Database` and click `Yes`

After this, the warning should be completely gone.

----------
VideoID:cnsk44ezy6s