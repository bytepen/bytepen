
![slide](https://lh3.googleusercontent.com/bz8-madAHhuOIX5KnNJcZImqfKVXmDXp87hc67PwQlsmPJd0jls54Da1wl4xNt7Hme9OS3LqIbWErkVxNLUhUE30r1zmnmamvfZhOpKpF0mwB99SnhmKhZNN7hZtwU0Y02k8Dls5cdEqna_5wdYh43Pcyxoz9TOn6V0ihEXkmZB4UrKxF6Q4sFQMAiAWkTeyJFLqQrI_gE9MZD_YK0eusZyMiCYLuPLpktdoWAwi-bRdmB_lzPSvaX1frgagOsnaWvyhN7YAP81AeSG4kyUMYOduPAhSivOJacTK4fhU6xpc9jpFq01H1F0i00k6qPIskt_2zRVjBCzmdPloBcghSqdcKoCX7y7RT1zzPsO1KXTKsLkr4A5w7fSn6TNhoodkXBbjBAVLKBXZBXiNPBsHqkoE7K3yNWRF4bZqb8uqgtPzFkIeHNL-Po6RaqPq2oDlAztT0s0MDsv2ToRwncB-5D6ZdaP-m7fXc84wOzqeVGvQuVuC-G_BbpshTcHpsflVMMcEU6sFtjK-8dPBotEIeHYnfZK6GkE6-NJW-msBeiOgt0bSR6SCkwThzmhxiJYyW3ZXo0l_5EzwYObXE3DWmN5Sl_17W555oBy417s5X1-x775rvfJkQ1FWZSHJoTSYyHI2S5jzuasjSVAlJWECP3ZfY0nz4vdUqzkf5S7wUopzry_NvnHhhckOjbbc5ZCSXaNjZAPmD_dnaPMi5YpNZ__1)

In this article, we will learn how to use
[Autoplugin by TheHeroGAC](https://github.com/theheroGAC/Autoplugin/releases) to setup SD2VITA
and PSVSD on our Playstation Vita.

-----

![slide](https://lh3.googleusercontent.com/kjVgqZFQbWarhDS_nZVKXX5Z5Qv0-Zpj3yVUj2bvtjaRQBWC5Cma4ac1ol_RgbYT0mjavHEdzxPBpg-0YKo2VHaos3-ih01_eFViulj3Kf01h72fnYs-Kuye1g76TIGdwMv-lKorhWWkVxVi4kdUaqk8q_Fq2OOeMArUSc4zVm_0p4-flFRi3fZzKaQtr87oP98zxFgqkML1zQW2VhZyfLEq4xnvxEY8i6qRJiIa7RA9zG6q3VBXzJUH2D-082wwyvkDrXOVI9Phf9ff3mFlFCnXSD7wboehYs03xpHyP2J77wrriJ6yerJvQz62wK9vunQIvh61XRFjsn1XGHhvZ6jrBPC-PwMKTDsWQNybuoGnDbV9VWtPfhAh2xjXkMS_OobVlAZZaURUVoLKTbq8OYijPRgMflh-b7vfjU4f0LSPiT0g3TUGg0RZuR9KASCQq8fXm6QG_kEs8Q0jLlAtSN88i-SjKe8opVyIzoJ1R__qBudQ0A5rSRrWe4xieioM3EA-V9TrG-9nKl17Om3MMbSWdotE-xwNawqx7gWiEupxi4mSX-aXmzp0bZVlE1vs2eOYZi9pEfvHYPSva2EdwNOUyCWVwkcaCB4Jg9zF9SXws0XsKkR0rxbdSxp6x4GZFRDLz2bNjORVxFsRr5FyLy7qcqWachBVRuuoyTYPcxw6nm0IIZorwQT5nPX8S12PXQ1NpIKGN97W1HMOUvgTjqMC)

For this, you will need a hacked Playstation Vita (either Enso or h-encore is fine) with
Autoplugin installed. You will also need an SD2VITA or PSVSD depending on how you're trying to
add additional storage and a microSD card for it. Finally, you will need the
[balenaEtcher](https://www.balena.io/etcher/) software and
[zzBlank.img](https://www.BytePen.com/zzblank).

If you would like more information about the PSVSD, you can check out my
[Install PSVSD](https://www.bytepen.com/hack-the-playstation-vita?v=install-psvsd) video.

-----

![slide](https://lh3.googleusercontent.com/ujeg-sJFcZbvUPFYkEoqlLZnrRHK4FAVz3hpnb_D0heMW63Rr7foRa-NggWzuV-YkFQX7jLyDnPlxHIgOvvHxpwf1zrClzXgimRs9NYBzQn3Ah4-reZm73x6D8scPwLhEX22t30B_1wdUb35g7Mv5F_GM9WU5luON3Nzx_YyR-qeOyDY2l3lEQdb0NGCWlvzymmBWfa2x4yjy-VUUIVO1Sh7xDy6E1PPzlkEY-pVGtNMiMsZ9r-uBpJECfpek8LiimYhhdMwhnMAIMZVaInU8Y1XWKa715IyNKzipLK7CR1BkjvsfgyZFQuPrie3SIRu47xiXert1GniQIKxWM15niIds1weJqkQHfMnLDqvBZypxuhhd58nHDvuqIkAz_0ySypg0qHCdeGKbXtjf32tVLJlQSdgtkxag82NLLjUSj5UtPHGmSTf02rMCHGcCGLE7Cjc-rkPNVB9bWxVkxnxxkjm_KfwjfUO8G208VyDHe-PWiOgQT_vSaknfoVsU3kP8peWdtLtlkqKrNJrGEtj8I3sW-DWSCoRaoDkI0bDCFqTm9fZ1QLhjujzlqhJJ4Z4qPHuQU5vlkKDLnVG9ZdvpJA3uCa2W5l7ms53Qvdh1vT-D6291LkPBa0N2KvEH78MUxAn-HUbdX9cdjnpzUv69PeX5xpH1WOnkX62iq5xnl5HYCvQqY94KU7NsJ7A-00WhA0a-_7AzdO51W2wY3Kf1oGt)

First, we will need to flash zzBlank.img on our microSD card. Insert your microSD card into your
computer and open the balenaEtcher software. Select the zzblank.img and your microSD card, then
click Flash. It will flash quickly and we can move on.

**Note**  
This step isn't necessary for every single microSD card, but it isn't a bad idea to do it because
it doesn't hurt anything and may save you problems later depending on if your microSD wants to be
picky.

-----

![slide](https://lh3.googleusercontent.com/C6moGGbog9vQdtHWRVfLB62yOWEYA5DQR4XcZ9p8wQZwqDRYO3Pr_FrIfpXP1j4fv18e9jvyo95O5ZNPJmUdpZSI-eg9-RPIkWRuQWkyxPxXknMc5ig9dC3qG584oy9Hhr-MZ_jq5FOB9lLP5VnXpDpQB-nXtN72ni8f74ggtX--9_z1MiK359PuAsDY5eVHKVdL9TzC8HuWYXsJKD0fDU4sF_OA-zhZb_AwYSxe0BWWqxKMZyCi3C3MX_4ycJhXUzRL7FAZtgAHObpQFNPKtodbjYFGCo-L_t6vTjppv7NlH8PNT7xEVzbAF4VA78c9DQDLnaUyULxBLUVG-eE3Jf_v7BEFOqoVKnzi4G0mM9kEC87HWcKOuP1OqVwt2na2lw7Pldwj7lcMIp1nFr5yekQCSzi5ur3K4CY9KFjJczJ4L5sCghwVgN3lVngAHfoMNNPSQRUANOFqHwXh3IL4ejoDO6PlMWP-kUA2kp72TBe0A3ET2pS4AXdyZ6XWAqzu_OBG0SW0fCQLpr9Y2MFvytQYd69lnzgjQ3UNKadW8SruRqzgobd31hg5PXQzZk-TSP1tVYWj8A3nNTik1z3OPEfTFvTAdmiZSIn6ofkOHXxymeDWwz6LXAu4GNFamsuyi7lti7EaVgrEuepk8-0hgJBCdZ44ki_U-Q08mE6jeorMaI1rrBAjbm_jgLegvyRLd90bs1qyuuXudriDlKyh6Kuy)

Now we will need to format our microSD card with the following settings:

File System: **exFAT**  
Allocation Unit Size: **Default allocation size**  
Volume Label: **Do not enter a volume label**

Put the microSD card into your SD2VITA or PSVSD and put that into the Vita.

**Note**  
At this point, you can copy all of your software from your old memory card to your new microSD if
you would like. This will allow you continue on with the Playstation Vita as if nothing happened.
I personally prefer to take this opportunity to purge the system and start clean, but if you would
like to keep every one of your files, check out my video
[Backup Memory Card & Transfer to SD2VITA](https://www.bytepen.com/hack-the-playstation-vita?v=backup-memory-card-and-transfer-to-sd2vita)

-----

![slide](https://lh3.googleusercontent.com/5-AVLPY8dvbveyNVDlWCAy31Mih80zO1jBi0OwZeeg6pP_RiUPRxSPO0NkN3yhJ8qdRGf6iDmwTZZD_TUcLGqtfDNpxkKbnvm4aGcqQLb9ThkfD7zMN7yHhoye3RunT8Yd_FCGtOTnOGSsV2vkBWCyWQRuaGDeQzT5i_Tyg6Vixl3ymTKRmEP4cVp5F_BXN3KWrQvrFCNeqosUVQeL-EHPuUHTui0JjEKuw6hOiswNBrCDaM9fYY1lTiQY49AAsph1xVebesESWdZtnKY5Ck4y2rZJOKMZbK-U4hEBK1zxqL2J-vSTSxuKGd3tL2ARWINVwLQG5QHEeRrxaMfUuE4dgNzNis3RXTZFLbqoCIOWKJtCkkJhcW1dtZtTMHAMFwWSDTwTOdIw1bW1j3x3RniXEvfm32Ps8mnES2cvdtuF5KMuiCHmWHd19IDsEtKSgcBDOT11KpTxxgTGgTgMJ9hca--nkbmRix_sTd88GQU89g4q8K3iW7mWYmXpg-7pGSZaqnjSZ55MOwwS91AusdME2kvg6xQXkdmfvNXBd0OKN42waP8Dy9nGQljvRrr8wctjzskpFl8vgPxTI_M6VRXgpQj2rVSQxKJmz43cRzRdJHSteArddLrhn0tTtGN_f8PLQuJyZwtdFGtleKbyLn9zvsCtTwfAOUJM8Pi9WO5I20pMG1koVrOuVFvqCK8k34J5pFKDbXiQrM1XCfiLNuOT4G)

Launch AutoPlugin and navigate to **Plugins for Vita**. You should see a **Install Plugin for
SD2VITA** option. Select this and acknowledge that **if your SD2VITA is empty, you will not see
any apps without first installing them to the SD2VITA**

Press **X** to proceed with the installation. You will be given the chance to set mount points for
your various devices. If you would like to learn more about these different mount points, you can
check out my video [Mount Points](https://www.bytepen.com/hack-the-playstation-vita?v=mount-points).
The default settings are fine if you are trying to use your SD2VITA/PSVSD as your primary storage.

-----

![slide](https://lh3.googleusercontent.com/lFahsPb5gSyojaLgOhp8Qx1fcXG_tVoyXbcdv6L75lp10sTI9_PK0jnFi6g5cBQ_vmtGNRAvRgzq2-CKCnCl3qjMgXm2keamP8fssWIDRMG5xqnFnTqPe7Zx-7y40k3_fnmh2iTgbgJv3ewF7yEkuaw8J5NBzN5Fcx_ghC4Noe35eV5jhhVn43AQx7uaT_Li_X9pCL6-87QXLLfw44gM_sfAHbLd6dAycIxMCCzuikv4u4lBH-JMruy9mzAIM_2Y1lkwYavSP_lEad_lEcOMJJaQLcwt6B9AmbG4tEmsPYmXdSojGYacuTcRPqW-js6K06T_qR5H_YFfRAZJ8P3ke2Mfvq9azM2ktbXgDMjgvTv6_wpMQjtxXUdIHw_I969Ils1KKKbcgT9emnonW_sqgilSu-63YclzpM-jJF-UOIijFpbbjDAddQtkv5HIaJWsiAJ81j0U01HuC3S041g1txiAfBsCjZ-U_6uex36qXcXBd1AN0nythvxVRpl-D_Mz5E-s55hqDksQEBNXQ884RCFWgVy8oXe4ji55Tn2udigjlr8_eC4cNn2RDTLyT1UON0zPb4vmb6TsCEhqnhFCgmV8Af43vyS6AsX7p8S5Dhaq_a5AKN0WB2k6W8TxdX0C1gC3DAl9k9K2rKWK-COcNMdbL6nkkJGJz-0TN_vB0LBuYwYZPUTYsRgL0kbueLRveMgJVQ5esHbrqaMOxplQ8-IJ)

If you did not put any applications on your microSD, you will be presented an empty screen. From
here, you can check out my
[Install h-encore and VitaShell](https://www.bytepen.com/hack-the-playstation-vita?v=install-h-encore-on-365-368)
video to reinstall them.

-----

![slide](https://lh3.googleusercontent.com/SSMuyQM63yV638FTQs3C1PeMAPpgfeLCsRYoE_rE-z9B8L_eRkn0Ke8oAQuKLUHrLisqcK2oN-qOc6GRWBgDHJgkqW3cIszgBpcyaX9ZxwjaSXxZEIlr9pqsqFgfOPy3jB7aFQobkfDYphby2EQUj2Xg0_t8y1v0yPDuDNdRcMLLXJn9v_gjtgLWpC2PAa0Ikwg5PILmmo5di26LvQqS-HL0SkuAv5E6PQgWDcI9Jln-MfQA1KfG6hzNBJ95uAEjFDPZIC4DD8UmuawtvHXvBmsnTrnJ5cuIukcMzEjjs6EcV6-Kn0ZubF5oZao9STW7FQ7rMsPTREzYfc1KN2dC2uttBMCZbLUB3beWe2gYdPjnDmEvsCeB_8Y2l1HANF28p3wRzYAyT2SogIuG-Fv8vJd4tUHckfv6U5eXwmyFzX64dNZyf6RptjgvS4JdSwMmZFQQYQAic3dD6pfEnYOfJ-xEip1zWm-rJLpLcR1MAbZmV3X4hawg7n799RVDknSvSjmTmBUB9r8GEOxajbrwjUXv-imXw0Mxju8H9mGtkLwhZvj9iiWtuw7agYhRu9zNbq0EzCrjVGqwLPiZEE27efdanG8bqHs2IVlblfK-IaGrqo2VJlCYpfzfVNyJ6vGXcu5ETROf4nBX6mqsPBeDGW2U3Za4hsgE6v_ofa8i-7M1iKhj0bXJMJ7GB2sBj5eFNp6UNhc7UV3O7fvpry-UtuPc)

After VitaShell is installed, you can install whatever homebrew you would like to have and it will
install it to your SD2VITA/PSVSD.

If you need help with this, you can check out my
[Three Ways to Install Homebrew](https://www.bytepen.com/hack-the-playstation-vita?v=three-ways-to-install-homebrew)
video.

-----

![slide](https://lh3.googleusercontent.com/_Lv3JZuK6YBXUkKTtI_ikOAZqs075hwHQ9bTZ2OiMHsG8Rqea9LQqqooWmvoH7RAYWYJCkauce1Ruhu8Ch_MLat2-IpKZVZkWzzLAEsbDNAijK4v_qAS62XjPMa0OMJ66mCHcDmgs_XY_rft3_OX6IEJJbpm-db9GMRb6RTKy77VEQi8VKkrUJ9-re3Mwp9KQcDYIsahlWqPjgfM0IM81aSO7l_uvUW7NyXUG1ioLADzFURdrt_HBxJhwtVIqkYp_RF1GanHFbhetzA2DBL19dkhVQ5uWtUPxGFVi65K3wjt2lZhez8Jkcs7muf_JrgaPGbsP8bgCfQmRrldwqTUWUboyUOViqSFFefmNghbUi4Nfwm1AG7-0jTT-conmyGBIDwLJ2-8YtK_nQrmT9fEocIrU0MacWBRkz9YHx7HAibF53mq_GLcpyCjD8wNoij9y-osyLcjRG00eYiUAh6IYX9uMKQqHvh9nsMyRz7ndx_RgPVund7WxNY8V9mKe6T7ea0CZQWPd2-knhmw_EPKmhj5FkXACHtXPgP4-D6Epq5OuQ4IoExltmSPtVCyveUWAW8RK-vROxG34JZBWroHzb_LcL7lrqWoiT_HwXQFAb15XlENJEdcPmgT5d_A8_JpSZdoUFRUSkhOpCcr9QdXX0eytl-6YIQFq8it6wAdo5Bz6wIXdjdtm5T3clrcpR4aZ6W8HKL4oW-qOVCprnic90p9)

Finally, if you ever want to change your mount points for your Vita, you can re-access the initial
configuration menu in Autoplugin to easily set it up however you would like.

----------
VideoID:1K9lkKUwpqI