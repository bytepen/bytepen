![slide](https://lh3.googleusercontent.com/qAtQuEyV1ZJ-Q4vZocJwdKA5UQZZLFYp4fvm6MsdIHeWIqzgVdiXrRzfYaYDAAD8k3_Betv2QWYHfBnGzMn-TNDFHNrxN2CgCxZyMa3x6Pb0s4ysa7inv7nLdBs-3GsGJEle98dRMGSuNNE24tnxqrdbxCihkoqkQU2OvSDagNe4mSIGMbD2NIk4FyteB5d8XJ9Yt6Foc1ItSRNiubH8EpPQ9P8XhnfhXhlVH1USVRB1txCfFVKCcDjH9EW89j5HDDsXVXU6jWkOn2dJbBznLp2wO7HJL0poAKn-Ong0CBwOLAJ5wXS1E3ZXf_AD1Nl5TAKDZYCzWkp2u3HL0Gz09yn0ON9wndTwN7vPJpZ4FCoh-0P04uQDayxniSnCXC9Ed6pTu1TZUtZb7YIpR0M5unFZzEtWzDXuo217_jqNDd1lVAaUQ-lH9vpyA451rmzn1ncJBQgoGeA9fiNVkoYnara_LxKt2qr2NvUVtOo5Nb9blqEsPnmRGwvDBYZFgkBYWh1UTGywMjUcm6HLUJUEiVxE2ADbG0pEiUL7v628__XFVGXeudAPGPi7IWO7M9MocqtTmDinYFDbdZa2DGk88zYh9MwbQ96-Z2UUpdArP5HDjXeDvQxGFeCch-Pt000W-MmAQVT6wP8E14FXUh77lAWOcBE-1uDKAJJts-aTrcJowRUqkCOjB5oysMq4nqVLr6_8sRY5isNubDhsWCBsFs6D)

This article will cover hacking the Playstation Vita on firmware 3.69 or 3.70 with
[Trinity](https://github.com/TheOfficialFloW/Trinity) by 
[TheOfficialFloW](https://github.com/TheOfficialFloW)

-----

![slide](https://lh3.googleusercontent.com/3cfehAysILZliMXhVNNFJmtslj-DKUmsFyJ13xBNVGeBQKR5CmCYkQ7TK_Xb1wNaGfONWUvIgPW4Khr2-6GymWF41soOFatnBtNi5gPhrT1OmbU58B1WA_i99gRvudQsXR1Frqc7GDTHmPSw9chS0rYV-rUE218j_e5mjNjK7rNeksFvDBpLJyWodMiO2FpxmJ1DYsrgNMIbo51bgz7zTGYxTpMXG7yM_fyWNFsIlzqyqfXgcUQsb9inh-8GUIB8adHncrbsoDZOuLkWwv4VQsyNvYRRgM_ualOpVMU7Yay8BXNsBJwSx13eLytatuoIWnAKqjKdfmdubImwhOEp9dO524lQZoGaijdVW8_R1v6sZJoVd21J1g1UoW3_7IhsV8LHEm8F5dByTmHTkADi0tREqd57WnJd-hLpgnAAilPoyizgsQ62cCfh-tM15zWUzciIjnsSw-xOGQuo6YH-6zFEFeFvKlmFlhNlT0-T2t-Q65z-bXqY9uk36hDOMvu1iSfAHwky_YfuTFQ6Urwg5ZPjmM2A5ECDwklWv9nE8qCk3lfOb0J-C5p4viHabeP5j13EkUXId1k0bcqHa36ruCDBz6gmHtLsXk_aBePIiYlzovibGQozZxhP5TWWmMPK2uz3Hhptiam_ASNtkXQrqfWHOevehV0-JJ5b4aLnLndhl9tudRx9hzzCme38Ehcu9NPdUxKzkw_n67f-eVX1yJcQ)

For this video, you will need the following:
* A Vita on Firmware 3.69 or 3.70
* A PSP or mini Game (or Demo) (PSN Account Required)
* [QCMA](https://codestation.github.io/qcma/) (Uninstall Sony's CMA)
* [PSVImgTools](https://github.com/yifanlu/psvimgtools/releases)
* [Trinity PBOOT.PBP](https://github.com/TheOfficialFloW/Trinity/releases)
* Memory Card (phat Vita only)

-----

![slide](https://lh3.googleusercontent.com/P_KqBpTx-Hcl5DY0Ju7QpZFPMv2Y0bPYh3EDxbJvP7O1JXlZJCjSAi3iPPCtukk81zC55LfvZzKkFBpGHksrftvOmrAsihIbyxfKkmr6dpX8H5oGQWpCpK73FDiZuY0xrEwhw1MiZoYb7hBk0uoDNDPVUij36sJlLzTC59ZVcWPV2VfDGwRY47Avs_ECr_-I5j6zzDjcxSsMEtS2kZevE7pK0DudSNbWO4Hhk1RPHceuQ253_YsolwgoPZK8WJ_mjZE7BIMcgKFuDM6F323-l4Q58wU44bXVi7DqxH3Hr9L91A-Lij3LyVvzmmcmEXiKfDFyJhTmtSS_DPptvq2FaPcwz7aS-HVMUo8g5Bn-f3esWFuwqFa31VpuMzJYFNIl3hyS8X-KwVP270xJR-N3Al1zDKHDcUgE4n3WZCYms15__0OMVlqx2mgl1aqdD_0O2yG4apbmuN4CgS0iq7ria5veppwLLjI9FPzAVfP-K7Mr-SE1eBZagYZyl6jTSWwlBBdj-vgtQm_Ok-4cl1YF45wjp_EpVkyTOjNl59dpbnOflNLk-hMA-g7aaVs1BG0EL-4gjFrm0b9NM36cmcNFT1cZhfo9fwaXsKKip5wzAXTgy4lVXweeX19thbJgIvyYZd_d4Wthom9hQbsBFCxiilRRX_0u5OCa5YTNCk3rc71vesa-9Bxx6IliWC1XLbsoB4uMgAvL6KDwRj3DfdhpUIqd)

If you are on firmware 3.69, you can just update to 3.70 and you'll be less likely to
run into issues. This is what I recommend.  

Alternatively, if you insist on staying on Firmware 3.69, set your DNS Address to 212.47.229.76.

-----

![slide](https://lh3.googleusercontent.com/uSWO5O6FLAdfNe-wlo2ojK4c2tZ0E-WQ7U-H0NcK4Urg6py5jGcZ9l0INLs2uJ8456sacvzAAobJiJRKu1tIhWlOv0DhdMAjI96PNTolBdbtoMwOrcrokrU4z81y8s67OXs14XuKT6xzMvEwHV_dIFxWhh-9viMOIGmsgAdhJ3qvZir0bYECuUz2RYe3sazqj_ldEqkOb2JDSc4eeRTQ9XqduvyonQDyr_F8w89Y7fyIy2EzZyjJwHQ5lWmmp4MoXgx4F8wEXQ7qSK_28RFQFLZLZZuL7a_64efk8lSmv1iTODIaH3TlBHJFAY41Jdy2cxgnGWt12fmmMcMI1hs0ELouLRQ9hzZmoHB3dT2kh6m6atEGiocnhrOxLR2xYRo7AAeSkSNwqMHjKkpuWtLlOZd5TRCwY9YtjSkT6-PENP39M53pVaP-d0PmngUuJ5c8alEX9ycBYaaDzL01pV5stPkfCPoy-ccTEgjGxcm5e2yUq131yL__oZDVvUc1s6mlGuyM6RWCzetpWjObUHBuDM93RZQmFJAH5MHmFUd5fjzmu7C8xgFcPbZbjoPY2lnXOYfa2ec7ktVdmn3713Iel4z9CwsHiBvaZ3H1VXm6c9IximyyD3zk0Tzi4f_aAlyKBHBIa8fR0_AE26QPsStIy3FMXvS8CUyO9Z2Wm0pN5qys6fZmpPjx5nPWhMBV13D3HdvBMJ_4vUVCdsUBYOrP5PvD)

You will need to download a PSP or mini Game/Demo from the official Playstation Store. Here is a very small list for
common regions:

EU/UK: Ape Quest  
NA/SG: LocoRoco Midnight Carnival  
JP: YS seven  

-----

![slide](https://lh3.googleusercontent.com/pYYk9b3Uju7r0rbb4y7NGdilN7hLc1J-KKhpfbSqBpahgvBap3M7LK5Inhr4GSzW6xkdzgWdyrQPjPMEtVV0kVA2H5HtExmZ9wFNkdUDx3s0EfLCYw4GywyU6veBakEITNOIylgEJaaK38L0WAZMV1apTLng59ZsTtrqU8hX8VtED8eqEyQV-YGcwhnagpdxYo0lQ_MRkxtQcD-TVGWrJkflETecc3yQZ96dZFiR_14I7RS6UQtaaOt_ISdxz101PTsjteM4Mq3bvZ8-H9Hx8LyHH1BTfisGqIH-Ycj3lMdbCvbOKxtHRmE_q0a3mvFF9wKYxn1o2XB4hbRMfKoMnEXR1uDH1i6VvvpVVXmsfi9JnwRulza3hVsWsxBAEV-fkEvDeHDFSx-M5oOKe9rHTKmwhCvjUtJjNjzDBQfcN8gr-64ETtqO4b8oT2bhm-n14OjsZemI1EipHobcl0ULIwp5ZMSLjR40rkx3Z-spbnzX_fUjOafma_pYqtW-KPMBfn9LrWVA2RWkBGQe_2Tn1Ko-60iqPQOj4Va6Xr7-Oyc9vBOR9FAd0BOFV-Zu3xbmJbhGzvsNPo1QDhwrGFPrQHRsc-5GcTXsNc7LpcKohg_V7Tlyzd8k9EdO30TCTw1owIfOKLKbWX3_swTyt5jGE4GSfnim7nLNqrjOXr5f3_8Ix392x3gjgXAJL27xMqQDAjum674jXUq-3nwGoOJ1T2L0)

Once you have your demo, confirm it will work by launching it and holding the Playstation button for a few seconds. If
you see a settings menu, it will work. Otherwise, try a different game because it does not appear this game is a PSP or
mini game.

-----

![slide](https://lh3.googleusercontent.com/E5z3BOlEMeDilMpzPbA0OluQFv11ngawWBdIHifmvjbz4Gr7COG1KfLJRZM_bBOcnwg2JmtSKUeZa_oqZi9c_MwQ1nzv6fv1zmuUHGCQUTqTzRTzR2IKw4xrBJPfBVL2Bge8_EF2g_Fn9-FzXsERttruyTsae4LInif-11QRV_ZSpk1Tp29LVRSrx8L8qtAPWzPpvdM0imSmplh05omvAmF_Cx9leZLe7tArLCwSsP48_1WVRnGa-nPF6SjjZKEudFaexaCjWXxG79RDzn1HysNTb4SL8dGHHBAyvXF4Mt7jhLY3kD4TIGaET0c6ubh8eBfl4ZgjnORew-3SoxvK2L9nfhiONc5xRbcNPDYc_Pk-yNfE9HNSzLuE1Iep7wTiZWE6QSUrAnPwF_Rg2S8b8ielPOdKdEaWTSztGAN2yICHEmmH4xe_DYskZFkjeBu0XrknZqLVx-MeKmgjWZm4fLHMowEnKHLhsWFtrzVoTwNfuuyYFeGUiJ0Y9zwwBigKstvQlT3tgCkMK-yrZRAhkNye9P0n9cfwIf3bQ0xRcIrI3kp8Gb0FWv85mdwd0RMI5Qh6I-LklSVou4bMLdFPDe-ksOEzMZKG2ToUGJT-J32QIKiejrKGYrWtQZZmbhhWzAxUzgY_APTe1NZWyyumtUW3sVbZpObFqaKgV1GGXSJZP5SMk4trAHDJsB5em3Jet6wIYuXg8U9P7E_0s2xVMu6T)

You will first begin by transferring the game to your PC. Ensure that QCMA is launched on your computer and your USB
cable is connected if you intend to connect via USB or you are on the same wireless network if you intend to connect via
Wi-Fi.

Launch **Content Manager** on your Vita and select **Copy Content**. Navigate to **System > PC**, **Applications**, 
**PSP/Other**. Select your game and copy it to your PC. 

-----

![slide](https://lh3.googleusercontent.com/-cXsb7uxGWnXSEO6bRQsibWCv7XbJpYKmDMQ6CKOHrnE71o-9sST9VRhGqSuXlbFlHiY6bfh1GTT9KvA6z9DPIi35jg_oOMxxDJs7i9zu8L0vT4ba1KPRMn9KP-j5aHhIxQR5J0_gwXylrJtwIuGG6_qbXAHd3m_zK_5Gw5MxZJKRexoCdMoe492QF-_iblQqL7qlpk6t6pa5JBEwOHFdPPoLOuMSOzJI_zDgBlTa2o3Z6J58c56jL0yAqBvKhH0gFeN99Ezh_Oo_FmzDoi94qOLg42SvSO-OS97fsdrs6RrrBZFj71WY1prZfjgrI7OP6Tvzo3DS5D62KPNP_0JgjM0ST7jxKSCqT6Ilh4r6Mi0SwBAk1nIqpV011bkrZfpShUHYCOrOaWHAVJEs80EgQt65RP4KPnPH6goCikURNa3f0OuVISC-VkPJpfwJvw_Bz5_s7ibiduSRQLniJpD5V6569V5E35KVaH1YEu48lTYoljE_25pAXxc0mJjXc_FK8qdWfVmGGRKr79il7oXR1Rg5lfrQuByyKMCTdXPy_YJU19q1PPapc-Cw9meO3M4KENtPY4S0OHNxp58nVJ_UCG2hN0CZkxWQ84nT1cqvKJcFwHkZQMAdxBJ-sFjfZ7n156YbayyNYo4Qv5odSX2CML5Xs5Gg3bE1hxLqtRroJTGbQrB8bDcJFoBYPxtXVghTfC8Pet9hQjOvxfoZj_PCru2)

Navigate to **[APPLICATIONS]/PGAME/[USER_ID]/[GAME_ID]/**

**[APPLICATIONS]**: Applications / Backups folder in QCMA  
**[USER_ID]**: Unique numbers and letters that correspond to your PSN Account  
**[GAME_ID]**: Unique numbers and letters that correspond to a game. (Ex: NPUG80318 is Locoroco Midnight Carnival)

**Note**  
You can confirm you have the right folder by opening the **sce_sys** folder under the **[GAME_ID]** folder and checking
the **icon0.png** file

-----

![slide](https://lh3.googleusercontent.com/M8grTKUjOdtwZ5fiIoSPedO37TBO-awEsvtcrHyi22GGAsn7DhvNBAFVTGXEpcKPjEm48f50WqVtHlqwFDEW3RmwJ2e42lK40JmHI81zETq7xRwv_b3tBuyFaAIQAqh-OGXSTvcF_C_tzw1hXFr5gh-GRlBbNjmqYjqxHSZgLU4RTt4HjS8Tn1lrpHNCDt559yVkMDt6M-IewOrhMCjOGNuPsfzab03bkAE6Gm-DNdllHtf1qw8nwtbTZ38cN0j9JYs9_InTAicuQUj752I-rkG-TFHveggF8tdDQKqbQE7YWI3PcDQiksihpV-smQ0z4xzRBi8nagyWaINNWuqhySrG2ycyzHlNpA4MUscSXKyqx5BjDwnlWen-mI_4XTUe-guF240xPJES-SpSmReoO6J9kQjhm3fC3IejrBE9CRNQtnB-ZxHsWm0SSOFmbLpSbtYRzxC_LycHBdfDRWQ-FQznxGi87YbgRUgqk3iWrV12kbj3z758RZFILdEFwWstSlVfVQno87LeUwPBRHI8ZLKYix1g6Ti_p1jOVt7IvgWcpGMAwZ_pj4zHH4qXJlLZXtMKXnfvuNAD-OXX7jPuTkFEr-tHP888zrIg5lMXJEER1Oz98UBsaWriAC06wMaRA6J5g3mC8XK1ef2Al5d14hQdKUvQitJD9jXYcqk4Fozv9zRzN0RgEFK6WBLnA2K1R8slm4FYYO-7fqREaRMau-_4)

Copy the **[GAME_ID]** folder to a safe location in case something goes wrong.

-----

![slide](https://lh3.googleusercontent.com/UF8WWgqunuISnrn7GGVOD8gd4UDzyFloedagKbohdCy5qE3S8GSAL5QQDcmzM6xX1quL0K0MRNmoPUAiTp5U7H_hbJjoL2137gQ8LL221pvFpPyrhNj0XwHZMe3jy2qONcHij__wEwGxAC6U2jPZmC3WpPZEMt8onVRUD3ViHWma8xX-wlV22i4Z2DHjrUDLzTxZEzghe1hos47qkFq4rBOMAU9uvyNrYedyZyb6HZawnBvZhDHr3XUTVUTDWj79vwZvjO04ijWzhttxushG4MRw4SHWOXWhUoYmi-OX7IYQ0SwFH6zQnOQUvph3kmVn-U6a9mZPLZ3b-0DcIvyDnhcUrw0ewSQjR3Kbv7JoHM3ZdxOJxi7q1sbYO5kKZ_OKoS_h3hrfRPXFrAvxRbxQQJTTsUGg-5_U7saA4Q1zhzpAs-L7a4S9wNbHxR25ciguhdbPFt3ZF3ad9kTUJEWEGPVi604OirGhXYUH6bYXmuZHWKarOG2fKOVi06Bw_DdseVo-CYx_QC-Myrhe2WKZcwBgGchz-wX6atk3EuyAs0iIJaIAuC10ngtpTj8g0bR-w34yHcSRczxpmtO8uTrFujnHPq-plR37-j48oIHDaALaHps-NKqwNXf4iBQkvnVbghuX3HAbOWywIwLar9bQgp54emKp2_lNNZD0KEVCFIPqliNDykaKe3-IH62fcH8NpY41eDmHDKW5dO0acIiJFcSh)

Copy the name of the **[USER_ID]** folder. This is the folder directly inside PGAME with the weird name consisting of
numbers and letters.

-----

![slide](https://lh3.googleusercontent.com/cXkaglmgztze0YUqekLeKay7EocytV7suGRA4HShtnMysyoEB7U390a5M3NL7jqYolXdFO_wv0L8KIcHjQ-biiCvueMuE7jzOqSO2cWw9KXbtfH3mY4rGHk5U1j9ldDOyUZa9pId3I6T2W5D7JHh2pwycZhW6N45TQvhQOuttXZpV21nsiVN9cT_sMxIB18YUoA5cOdx1MWxIl1-klr8P7ZwN_BBkFf9QpY4M75Dlu3KPifS_N0IwkQqH9yUJqE_RAQvdT49hUU41JtqBpkG1Lv4qWP2QtQZQuFAPMmBHCg4lAJOfUkkHdEFZ0uOUp1g7HHQIkBC3ham2tViaMwMYA2qGhctRZ3HLisG58B63MupCv_73f9_jmjQXuHDbpAgtHAD10t9R4oLeoolKrdVSPh0jhnmduWbzaiWesC7NZiTYrDzX4rtKehMy_O8Sd4FsYAEalj53jLQU0VMSiwSWehFJrvlhxDb0dHjNvHDJUZ6CA7u6G_TfxeqMBdwWFec9g2FubteywUYCVtN1zGiC6omJ2EcoH9bh92HTGIv754R06ghJ8cvVbB__q5wcj_WmyQkk7Gw8x9ROp6znH7wEAFkdPmnygiTODkq2-GYtLh-A0FS9rTbIWT8ulbuY9EnatR9tN2NOSeMVfnf0p-_R7VLT0KrbMuJF5YIx_oEvryVi921pfp98gwvXhLhYLtIVm5wdAu3XqGepazDits_7ugp)

Go to [cma.henkaku.xyz](http://cma.henkaku.xyz), then paste your **[USER_ID]** into the **AID** field and click
**Submit**. You will see your **[USER_ID]** followed by your **[DECRYPTION_KEY]**. Copy your **[DECRYPTION_KEY]**.

-----

![slide](https://lh3.googleusercontent.com/r_D4NryiBlV1D_ZyY51NBYtrYNdg80j6-NPNGoYUerN_2bPQSqW8eqSoaTpwydiDJ7LrMGX7AD7jxh7k3T3RJZUYgBZ-H0C65GipXPdIQbQz7ZMTjxZ6qhv0gMmAedqvdcNXfueuLV07MQ3_uUegnrQOQftdmhHJ2IuDyNjrJcZcebaQ5s1lEk7wQVclgVvNUBpBZ3mmQCjZ4_iRp__Tvp67EfIfim1n0BNU-IpxcD3mTyxe_c65lehQ-flOSoLWUXAU4Y9Z0xqC51zOkvmBaRmALN3fuds5KYymqm5581Ye1wIqxP9rHmSIyZdKfRDE26005LjkofSAbNhF68frpTuGzo-_BeXDoxGMsXGFNcldShCs5wA4NYd9MZAM5a6vxfCo3c74uuv1aZBn6dxXFMOtudXveLw5_n3zqA5S6GXjNdnztumjJF4Gbc70VmYJ7R2Sl4hkRxy0Rd9HOzBJp9cor97I17iTfUwcZoW51gSuID95MBdoEiXdQehGL-9ec_HSW2Q_SAG79CheG6SD7e7pRiJQI-ZyvnFBIfkdDVGCkWlDODCt9oiEtyCyUAlKT_JxQr5ErNl04rzy2Y0fg20r6gtHIxP5io0OhtImmeCFXoyeYb4q5NyCi_OhBHgduQwBwpa3CWuy8effY4q2D76o3uewDVK0dVyd8Z8o2Hco1F1xPhnS-oV-QX1hat0nIyj62xkwuDyOftPk6pywmFIN)

Copy all the files for PSVImgTools into your **[GAME_ID]** folder. Hold **shift** and **right-click** on any blank spot
in the folder. Select **Open PowerShell Window Here** from the dropdown.

-----

![slide](https://lh3.googleusercontent.com/Mkq3ay-edsCEi43To8cfoV595zGCYcf2pI4mOCinvaTypwS8WSL2bN4gMxg8X2_sT_xukBds_jieMkJs2JVeMRQkLmO6t-jfcFi1JM8NYGeZctZYBzgYLKFTXqDZkUOWGXrvrTFP3ioblTMnLfT9d9DE6bQAjlfIBwlGQV8dLOEJDS37KzqWJ9zB6X71INLCAwCLxEB2BtzWZcHmM16PVNztAZqTSMaIpdAMQ7NQIYKEclYHCVTD6pPzr0uG9FZ4MSPB_UCtp4udxrtglWGPgLLRl82nGCX5P2PkCYta8_roBub1VPND_7VgSwqqZfKVzDgAtYgj5NnzwCoPz0LDgKxtAQO-wh4NBBUxfC00Ej8jvWbI1T1a_Zpo1IGt_LqTzMS4H5jkkZlnbadjmN-Ccb8LoF7E_jSLNRDDQ54ksdBv4bbY4aoLDRlu12tulWNsukwrpQgz00en6fQJ1ntjbHtK1mfQqVODiSLh-h3QjAs1o5oJ2gR_uiwJMgyVexzgxGepy0PmZDn42uxV_DkTe_F6-ZNWYOlS-NUcH30ioaoNA5-MaAJUhyLMiOfy10NEux8hrvwMhztfVn5WdySVQiEdh1Uk20KJQbp3b-tH6csZPzEN7pHRD7uOi-e0IMCSP3p0o5wDz25binrMw0OvW1OQREEXO3H4Wd1JRXZ6exj844RRY6YBGx5poyhmIb9b5DC0oVLa9VrZCA5mVAH_iOz7)

In PowerShell, run the following command:

```powershell
psvimg-extract -K DECRYPTION_KEY game/game.psvimg game_dec
```

Confirm you see an **all done.** message at the bottom.

**Note**  
Replace **DECRYPTION_KEY** in the command with your **[DECRYPTION_KEY]**

-----

![slide](https://lh3.googleusercontent.com/RJ5k2mTs6Ar0Gz7DJgRkNDi9evmzFq2b6nOPBsARzuYTHPVGiLe6Kq3-Zf2QtmpRoeXCaRE-8nXuhoC2jru8e_18mjevB-3nQXYN1GaiRhtSx4MLzfXiHJQIj2TL30OvACDD1kXx8o9uLLz3fY4Xe2GvRMpErph3bgvyrn3CNVd6cmGsQ5gG5H2wEVdjErr2kFKkmsAv72UUwVzh0n7SZBAApMcNopab7ZHUTjePfC6Oea25WC4RR8qntQhk0b9EgXkyAUC7WztbQDaoQcMivSPX620KfwYC8qey_94WPSy2IA634waJBHBDKLmUrReNYU9-j7GXYoGlp77p_mMVR0TjMSGwbiUn88HpTcygNKiERxhPKxw8ckKpl9DWf1PlLuYODTFgXEpnRm2ylfu89xarNzZAlezqoN4FvMep1t_uaSvW_Bfi7nwhyz2UeHIlDMdv7TRmRYagfyHRiDICRRj2P-BKDLNK7xAs0u4A_f_RCycDWfmodta48FsM5uSkEOHwbvfrWE3pwiHvTH7Ej1o3d-5ZDRVGNPGoUSVFR94wAtVcAmr2Aini1UBJPGg04tsTEAwO3Z4RAwZ8AadeIYQvBjKuOvwpnUqashBhAjLKRyWFysJtH7F-vg6U3YVp5kzSHT4Fpgf_087KZXaoYiMWolEeHzsqll-DKvPOeI64ZDUkw7ab54DiCbjm0J6NL1rY9WV0BilgZ7EfzvyfA-6v)

Navigate to **./game_dec/ux0_pspemu_temp_game_PSP_GAME_[GAME_ID]/**

Copy the [Trinity PBOOT.PBP](https://github.com/TheOfficialFloW/Trinity/releases) to this folder.

-----

![slide](https://lh3.googleusercontent.com/Q2riNUmXn0SPF6R3Bh8cKX8IcI-tYr-jJEJloyUeYjKxF0pSCqfOX28KI2d1bot3e8G37-mTG2zeI-Xo54X-60dH58jiwp19in27xkRMGFAUpGifPiU4Iq2ZM9X_iNBrmUiOcWG9RzXnyo5OwSbriiRO2mwsFgehbEM4NFfjBbe0B6PYg8BfKswVxWY9w0UsisDQVJOlDFpxrWz_U9fPzMoO6jYJ7uB7WC_DvEKUzVaqrsBGt5dGr9hM-P8oI9NP0NZTrTK_4AthshdOxWphDE6lH66Re7uVVm5dVGkFuDtk4IWUvC2gC3SXDQr5nf8Flba01lUCOFVraCMbTyK71pp63l3I2hrGP4kcr35WXJYlJ4KoFUtV-IUKStv118EL23IYguCM19HAOKXRBTLfkU18-hz-ApnnqSFwcyIc0_uzZ-2ERSnwVVpKnZx3G7HYmfZ1jIkuQLzUajKBMuPyAhH6hMpQV3znLVfpdIaz4MH_0_x62sUfc3CdxjBYZzajBGWF3gTVoONfofj3tuXScIW7qfBXu38mOkw-xMaxsZONr00pNJfCeXmMvk3z7m12Bj-IDJ8Ko22XXcIjCmUjZJ8ZBuFP4yxFl5eSj7pKHj5NFhyC01aL8lICosw2teyO7T9N7KoCav2fuMC8je0YWI4lPOeUidSd-HsmSd-O3BCgz1fHYUphdEpeIWhtBAAMXqojRDEO4b1nbs6lAsbriHQU)

Back in PowerShell, run the following command:

```powershell
psvimg-create -n game -K DECRYPTION_KEY game_dec game
```

Confirm you see a **created game/game.psvmd** message at the bottom.

**Note**  
Replace **DECRYPTION_KEY** in the command with your **[DECRYPTION_KEY]**

-----

![slide](https://lh3.googleusercontent.com/f6CarccxFcb3nPg04YKBmrjF6tJaCAwyt1gfTjw5mqI8KzHmDp4UEvfflZ6bhCcL6EHXNI7P_S_DvzqxLSEoybzuMobUf4RnpPkCiNv0lELkuBrB4U6IkPpXHkBSqhPj4yltj8kAQ2eYIS6eyCIhukf3gCtmoJfOdtVAJna_NAKG_RhGiL692xwuLCAIpdfevyqABQSt9xdfBJgG9tuE14mxMJu2Jhvq4NqckW-jLJZ5VwJZdXthDh9O73gD8cC5nbRgnAXdjVrm9tbs0T1hSt8tjlGxvcGCO-aDq0Fl-60Z3MmDbbji5w9a5YcwojRZS7qy_9a8uLjTKD9mrUHenV39GmMnsdhtUzRQeYAbRJhQwMuiK6qnp9JbtWEak7WR-uppjw1PLGGuIOZr1fLespDL2fT0mZYTAT8yz_kTho5jcW1WdrbUA7seBJJHBucXvoeih7_YnVQyAu5rHpOgpNqu-OPg59_zWzRAdNDHq7VHGn33afH4FdHQAmttGw9fCU9CO2cUFlGmmM9EdojYDzpLaJeqX-9Gjk_co8v57vx9mGQf8MGqaBLtNQPmawGAn8igGlZsXeKcWm1Hyn6QsLJ3Nzet9g4o7KWZ-V9J6Ukle94LwAuUhRzzc5v9Jr46Q-XJboKyygasjTxzposwzS8gDlOf-FfUZ8eWKmehJWf2bmimDUmlGK2-HU8iNexS7qLs4qkKlPpjxoau6mrD21jN)

Delete the **game_dec** folder and all of the **PSVImgTools** files.

Right click the **QCMA** icon on your task bar and select **Refresh database**

-----

![slide](https://lh3.googleusercontent.com/2AG_wbDIvL08LoEn_PsqWOoj1VCIlyhB6KzVL1x0XgusHm2vw0Zp0sRKbRseMhfCUbXb8YItXUkMDAp_riWqCFYvfCpIGUK6El6vzatwxbupkUH1zdrEKDqYrPlXpJq3bukhmPFeKj3j-8lyu9DdqSUlcx4kJc3JrLcRjw9mwOxlHuPn3XhN8xlEW4f1Heb0kGLf3IKneqz1nMhEM4Kr8LtTqegPQlJyLvQKgH-ZKMUO1Fm9SEe0azFjxUIXi2N1qkBt34xKC4OM-nW3253cvDncfQeSpVrlauAx8UGyIH7fL-gKvPQc8qEO6VaCIN15nvBSOY8_KxstH1_4UU7rYuNOCbnHudxE_x72WFWTrs3pncFHU9rZzbL6S2DJPimpP0rTE-QZSRmgCeSg1RHGXGk3S60Qx9oUaXBoNTlR8McEQi0ANlD1joxw549z1z8-CCabNpozYne4a8U1jvbipUZV8Cpj15FzOs5rWzJLESUuyMDSRAFjannBxBu5mIWfZzrkf7p4D_v4ckJpXKsPe6JxGuI3HCB_Va8ui0_fbI5jMlBNHsdX3kH8pDvaQuj7O8iiLQQRvtU_k-AgA6TabBWPl2IVIMeYe0VwXii7iWyk9wKsZREuZ7Mfn1DK4uceNNbtS2EtfC7jfasdYpTYhkviWohnIE7zGU5NtSN8pOZpbTtTYENYCn3EmPJfAwRLGusYU9uvgMGTn8gRXAWA7VNF)

Ensure that QCMA is launched on your computer and your USB cable is connected if you intend to connect via USB or you
are on the same wireless network if you intend to connect via Wi-Fi.

Launch **Content Manager** on your Vita and select **Copy Content**. Navigate to **PC > System**, **Applications**, 
**PSP/Other**. Select your game and copy it to your Vita. 

-----

![slide](https://lh3.googleusercontent.com/XTiZ8DAZEBgMV76gaWZJDSFSjh_xQj_VDubnNYWXU1VYZCQbms57bu-KllFswzYD_Nm3beA93KolmppDdJBeBdxZezY3upTYY36F8HKqzUdsEP3XaeHfK537HQn0VoRh6Mak1TVVc0toolpmN4dLeNJaoK2n2HdPeQ4wh2aDZmi-WXXZylAcRFUbkejn4jJYDcyo59p3-8o-a28RIkEFTcFDe-GHiYnqKfyoc3PXip5DWvjSfIqD0Dp9C4EYq3ujrSwqqK0e4Nmdqn2a6ZbMpCiXI-sCtXdC61jzb7Ru598diRyLo9UsZ4L3VRLJRiw6zuM7WkDhzuOjr3ce54iUqz9E0I49SfJ88c0RxyGI-iPlop41ibO5BDEBa8K8NuKmAcTia6RaKQWuvdNbUjNjol2rGdCM3fPT3NWrHrfUx-gsPFsGFvmBjNJHdTiNczoebOhGdRwJxGe5LaX8mqDZGnAaVfcAFxlpa9SNPWfPbZuJxgE__lqP1zfhedH-Z-uOaoWwC6qVC-Jx9rLz036gUbS5aIDEHOjBYVXX0V-33jxGvP7-VpE06G6Kvrdjco3FemutTZ2zoWp3UYpZpvSsSFtwEcxibaI2rNJ9dSN-7Z3lTOK0yiAgao5V_Q0-5S-kImYX2Ei-T4hxthBw73K7jUoDRIXEDkKyI26kYjSazvfi7ZHYhdhilmIBpxxBqQ7mZuDM-lTH9095kAO3oZeUv_al)

It is very important that Trinity is the very first thing you run after a boot, so reboot your Vita.

Additionally, you need to make sure that there are no active downloads taking place. If there are, either wait until
they finish and reboot again, or cancel them.

-----

![slide](https://lh3.googleusercontent.com/qsX3EvKfPStqvOJx2ZQyGROPmJEG6bsEhfXPVll021cV_7ULXrZTPWzKiSQVEO6UWdKdNeXpx83pItTz7OBngzORT7Qmb9qlxBdYyog3D4sZaAvaE2qYsFjjooa6pnRN-akflaGDkdPVM01r4Ekn-K-2wL2YvhY8jxtLeyvuGiL_Hbrceu7R-TICAaiFIYGmJQ8t9HxL6Ty7ujnyva-KmfvjWXS-kGMUhm95OJkLMDx5gTawqu_4pHsuLENfbeMiRzJDIgGFOnRoFTIsrH9kMOKIhRTXtEDcqViWx_IjqCEhBZ2rEvhl2Ivhu2MwPzTQsK0cCWrjCbYxogwJkqqjcR1mM6gEHi1HDS7jsKrtDLkHeANLCDFEVSNAHUiAaXI7Flygx-oh_h9BmLgvEVCcA2jSy1rY0L543OV0OJbldZ7hIzyA2VrjhBubJD7k0NdW3Sq5OkY_TemuYzjOPZpb45hBBp-FCY8Z1CoGmKPHnbxu8i0wK8afpzb3YzP7jlIr4uWUFYSp-6gGQPsLzFFHTYfC4m14aB5HxSaWsH1H-XO8ofTGjH6nm3Xy_gluU60eG3vGmWV0w8LfmJw2mYVKr8ElaUSA8TNjOxEdxz7ZOd3ZGHiMtEDL-prOfYi34MKSdH8usNmqK93mKYCsXoMrx0WVM96rYyq88S4CMW_jrKLnXJEyxiz9qN2GCvwG9cx4kEr8LoqrQ99dFtYyodveQLtm)

You should now see a Trinity icon on your Vita. Run this and you should see a sweet splash screen, then the
**Construct** menu should launch.

Select **Install HENkaku** and insure it installs.

Select **Download VitaShell** and insure it installs.

**Note**  
If you receive an error while trying to install VitaShell, make sure your WiFi is turned on.

-----

![slide](https://lh3.googleusercontent.com/v5leKMTKvDTBiL87x7fs2cYw2HtO1OO0zbLEXwDgF0sAGsRWPHhsEJt8OvjoVjrLmdPRK7Xw4YztvqV745-eC4naCfwK3X2HWFANttF7L0yGuoHD-BLBROEirrm6f29HuVE4dEI4XzCZ_dRh6fSxQ__uqPvaemqoDDSGWE96gSl168kzhy_vxa8DweOxzkuNU07gaAkoQyVH-On7DAmpE2xvaY6NOtu9ZCEIL5I_AC_9lsdnaIgYPapxR0s6cOQzvoG1T3qKOIGmzaSR0zoJT2l4sbLSgJlyXi81FvxEMT5OaJKTMbQsd70cOddaBss-BUJq0274xejUMv8Q08dUUauasHKG9RRv5huVyWq7vsXlBd0AIs3t8qhkHk8rFghPDFFt9tNsZiCs6cJOgKRitWIDRHjfxPEDS5e58vZFsHocRkaU84X2-LK-P1Ko1avoUTUpsnZQav9sXBKOvhyU0iYALa907KWyYdSLcXk-_vs6YpjtPszOjIJl-10aKyr53-6ylzsdwH-KoDf5AcPxhKdKQtSjQW3fXSitLZURkmiaevgRU_6gaQaJ_xdu2hnCEaAA5KG_iNxeFi76LsVLPwuzCzqvKqO4evLLZo4qk0aY4HWdyhrRroEqfhHDyFztlFPmPsL4OQrJSuVR7638SouHJxbONS3sFKRM67qOMI1KQv6hQLwjPQXVGqyqyLl-lU-Z9BX5eVU-goxs0J1M0zk9)

You should now have HENkaku installed on your 3.70 Firmware.

The VERY first thing that you should do is downgrade to a lower firmware. I personally like Firmware 3.65, but many 
others recommend Firmware 3.60. Either of them will be better. After you downgrade, you will have to hack your Vita
again for that firmware, but this will ensure that you are able to have a hacked Vita forever. It is currently unknown
how Sony will react to Trinity, but it is very possible that they will make an attempt to stop it. Older methods are
more resistant to Sony's attempts.

You can check out my other videos for help with this:  
* [Downgrade with Modoru](https://www.bytepen.com/hack-the-playstation-vita?v=downgrade-with-modoru)
* [Install HENkaku on 3.60](https://www.bytepen.com/hack-the-playstation-vita?v=install-henkaku-on-firmware-3-60)
* [Install h-encore on 3.65-3.68](https://www.bytepen.com/hack-the-playstation-vita?v=install-h-encore-on-365-368)
* [Install Enso](https://www.bytepen.com/hack-the-playstation-vita?v=install-enso-on-firmware-365)


----------
VideoID:OH9ZkMqJ6MM