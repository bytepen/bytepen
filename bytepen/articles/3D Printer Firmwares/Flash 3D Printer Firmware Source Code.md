In this tutorial, we will install the Arduino IDE and use it to flash 3D Printer Firmware Source Code.

-----

For this tutorial, you will need to download the Arduino IDE and the source code for your firmware from the following
links. If you're having trouble finding the firmware for your printer, let me know in
[the video's comments](https://www.youtube.com/watch?v=O_5V_Bp1MFg) and I'll see if I can help you find it.  
If you arefeeling extra adventurous, you can download the most recent release of one of the popular
3D Printer firmwares and configure it to your needs. This is only recommended for advanced users and may take quite a
bit of tinkering to get it to work well.

[Arduino IDE](https://www.arduino.cc/en/main/software)

[Marlin Firmware](http://marlinfw.org/)  
[Repetier Firmware](https://www.repetier.com/documentation/repetier-firmware/)  
[RepRap Firmware](https://github.com/dc42/RepRapFirmware/releases)

-----

First start by installing the Arduino IDE. There is nothing out of the usual here, so just click through the installer.

-----

Connect your 3D Printer motherboard to your computer via a USB cable and navigate to the folder of your 3D Printer's
firmware. There will be a file with the extension ```.ino```. If you double click on this, it will open the Arduino IDE.

-----

Most 3D Printer motherboards are based off of the Arduino Mega 2560, so make sure under ```Tools```, that ```Board```,
```Processor```, and ```Port``` all say ```Mega 2560```. If your board is not based on the Mega 2560 here or it is not
showing the correct options, change them here.

-----

Click the ```Upload``` button in the top left corner of the Arduino IDE. The sketch (source code) will compile and it
should automatically upload. Eventually, the message near the progress bar will say ```Done uploading``` and you are
finished!

----------
VideoID:O_5V_Bp1MFg