In this tutorial, we will flash a .hex firmware file to a 3D printer motherboard. Before we begin, this process is a
little annoying, so I prefer to use the source code if possible via the steps in the following tutorial:

[Flash 3D Printer Firmware Source Code](https://www.bytepen.com/3d-printer-firmwares?v=flash-3d-printer-firmware-source-code)

-----

To do this, you will need a Windows computer with the Arduino IDE installed and the .hex firmware you intend to flash.
If you need help finding your firmware, feel free to ask in this video's comments and I'll see if I can help you find
them. The Arduino IDE is a straight forward install, but if you need help, I walked through it in my
[previous tutorial](https://www.bytepen.com/3d-printer-firmwares?v=flash-3d-printer-firmware-source-code).

[Arduino IDE](https://www.arduino.cc/en/main/software)

-----

Plug in your 3D printer's motherboard and open the Arduino IDE with any project, then go to ```File``` >
```Preferences``` and tick the box next to ```Show verbose output during``` ```upload``` and click ```OK```.

Click ```Tools``` and verify that your ```Board```, ```Processor```, and ```Port``` all say ```Mega 2560``` or whatever
Arduino your motherboard is based on. Click ```Upload```.

-----

After the project has been uploaded, you will see a command that was used to flash your board at the bottom of the
screen.

Mine looked like this:

```powershell
C:\Users\bamhm182\AppData\Local\Arduino15\packages\tools\avrdude\6.3.0-arduino9/bin/avrdude -CC:\Users\bamhm\AppData\Local\Arduino15\packages\arduino\tools\avrdude\6.3.0-arduino9/etc/avrdude.conf -v -patmega2560 -cwiring -PCOM3 -b115200 -D -Uflash:w:C:\Users\bamhm\AppData\Local\Temp\arduino_build_45564/Marlin.ino.hex:i
```

Copy your command into a text editor so you can easily change it. At the very end of the command, you will see something
like this:

```powershell
-Uflash:w:C:\Users\bamhm\AppData\Local\Temp\arduino_build_45564/Marlin.ino.hex:i
``` 

Change the directory in the line to the location of the hex file you actually want to flash as show below:

```powershell
-Uflash:w:C:\Users\bamhm\Downloads\MyHexFile.hex:i
```

-----

Now that you have your new command, open PowerShell, paste the command, and run it.

AVRDude will display a "Thank you." message, your hex file will now be flashed, and you're all done flashing.

It may be useful to keep that command somewhere safe if you plan on flashing the firmware a lot.

----------
VideoID:4ei1vsmkOK4