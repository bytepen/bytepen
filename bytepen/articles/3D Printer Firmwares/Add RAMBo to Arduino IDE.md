The [RAMBo](https://www.printedsolid.com/products/rambo-1-4) is a pretty nice 3D Printer motherboard, but it takes a
couple extra steps to appear in your Arudino IDE correctly. In this tutorial, we will set it up.

-----

For this video, you will need to have the Arduino IDE installed. I walked through it in my
[previous tutorial](https://www.bytepen.com/3d-printer-firmwares?v=flash-3d-printer-firmware-source-code).

-----

Open the Arduino IDE, then click on `File` > `Preferences`

Paste the following URL into the list of additional boards managers URLs toward the bottom.

```generic
https://raw.githubusercontent.com/ultimachine/ArduinoAddons/master/package_ultimachine_index.json
```

If you already have a URL in this field, click the dual window icon to the right of the field and paste the url so it
is on a new line as shown in the following example.

```text
https://www.somethingelse.com/boards.json
https://raw.githubusercontent.com/ultimachine/ArduinoAddons/master/package_ultimachine_index.json
```

Click OK to close the `Preferences` window.

-----

If you now go `Tools` > `Board` > `Boards Manager` and type `RAMBo`, you will see an item referencing the RAMBo
motherboard. Click `Install`.

After it has been installed, you should be able to select the RAMBo from the `File` > `Board` menu.

----------
VideoID:0F7amkpVlVk