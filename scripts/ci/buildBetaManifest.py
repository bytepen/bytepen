import os

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

with open("{}/../../bytepen/build/manifest.json".format(BASE_DIR)) as s:
    lines = s.read()
    lines = lines.replace('logo', 'beta-logo')
    lines = lines.replace('BytePen Studios', 'BytePen Studios (Beta)')

    with open("{}/../../bytepen/build/manifest.json".format(BASE_DIR), 'w') as f:
        f.write(lines)
