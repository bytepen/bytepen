#! /bin/bash

curl -X POST "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE}/purge_cache" \
  -H "Authorization: Bearer ${CLOUDFLARE_CACHE_KEY}" \
  -H "Content-Type: application/json" \
  --data '{"purge_everything":true}'