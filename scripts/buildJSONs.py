#!/usr/bin/python3

from os.path import dirname, realpath
from sys import argv
from py_classes.YouTubeGrabber import YouTubeGrabber
from py_classes.ArticleBuilder import ArticleBuilder
from py_classes.RedirectBuilder import RedirectBuilder

BASE_DIR = dirname(realpath(__file__))
JSON_DIR = BASE_DIR + '/../bytepen/src/json'

do_api = True
do_articles = True
do_redirects = True
include_tests = True

if "--apiOnly" in argv:
    do_articles = False
elif "--articlesOnly" in argv:
    do_api = False

if "--prod" in argv:
    include_tests = False

if do_api:
    yt = YouTubeGrabber(BASE_DIR, JSON_DIR, include_tests)
    yt.write_json("Playlists", yt.grab_all_playlists())
    yt.write_json("LatestVideo", yt.grab_latest_video())
    yt.write_json("AllVideos", yt.grab_all_videos())

if do_articles:
    ab = ArticleBuilder(JSON_DIR)
    ab.process_files(BASE_DIR + '/../bytepen/articles')

if do_redirects:
    rd = RedirectBuilder(JSON_DIR)
    rd.build_redirects()
