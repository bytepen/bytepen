from os import listdir
from os.path import isfile
from json import load, dump
from pprint import pprint
from re import sub


class RedirectBuilder:
    def __init__(self, json_dir):
        self.JSON_DIR = json_dir
        self.PLAYLISTS_DIR = json_dir + "/playlists"

    def build_redirects(self):
        ret_dic = dict()
        for file in listdir(self.PLAYLISTS_DIR):
            current = self.PLAYLISTS_DIR + "/" + file
            if isfile(current):
                with open(current, "r") as fp:
                    for video in load(fp)['items']:
                        video_id = video['snippet']['resourceId']['videoId']
                        url = self.build_url(self.get_playlist_title(file), video['snippet']['title'])
                        ret_dic[video_id] = url
        self.write_json("urls", ret_dic)

    def build_url(self, playlist: str, video: str):
        return "/{}?v={}".format(self.urlify(playlist), self.urlify(video))

    def get_playlist_title(self, json: str):
        with open(self.JSON_DIR + "/Playlists.json", "r") as fp:
            for playlist in load(fp)["items"]:
                if json.startswith(playlist["id"]):
                    return playlist["snippet"]["title"]

    @staticmethod
    def urlify(title: str):
        title = title.lower()
        title = title.split(": ")[1] if title.count(": ") > 0 else title
        title = sub(r"\s+", "-", title)
        title = sub("[^a-zA-Z0-9-_]", "", title)
        return title

    def write_json(self, file_name: str, content: dict):
        with open(self.JSON_DIR + "/{}.json".format(file_name), 'w') as fp:
            dump(content, fp)