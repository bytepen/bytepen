from os import environ
from json import dump, load, loads
from urllib import request


class YouTubeGrabber:
    def __init__(self, base_dir: str, json_dir: str, tests: bool):
        self.BASE_DIR = base_dir
        self.JSON_DIR = json_dir
        self.TESTS = tests

    @staticmethod
    def build_url(category: str, fields: str = "", order: str = "", playlist_id: str = "", obj_type: str = "",
                  results: str = "50", part: str = "snippet"):
        base_url = "https://www.googleapis.com/youtube/v3/"
        channel = "channelId=UCvEimZmO3jz70KCd7W1CJZQ&"
        results = "maxResults={}&".format(results) if results != "" else ""
        api = "key={}&".format(environ["GOOGLE_API_KEY"])
        fields = "fields={}&".format(fields) if fields else ""
        order = "order={}&".format(order) if order else ""
        obj_type = "type={}&".format(obj_type) if obj_type else ""
        playlist_id = "playlistId={}&".format(playlist_id) if playlist_id else ""
        part = "part={}".format(part)

        return base_url + category + "?" + channel + results + api + fields + order + obj_type + playlist_id + part

    def write_json(self, file_name: str, content: dict):
        with open(self.JSON_DIR + "/{}.json".format(file_name), 'w') as fp:
            dump(content, fp)

    @staticmethod
    def query(url: str):
        with request.urlopen(url) as response:
            encoding = response.info().get_content_charset('utf-8')
            return loads(response.read().decode(encoding))

    def grab_all_playlists(self):
        fields = "items(id,snippet(title,thumbnails/medium,description))"
        playlists = self.query(self.build_url("playlists", fields))
        if self.TESTS:
            with open(self.BASE_DIR + '/../bytepen/test-json/Playlists.json') as tests:
                playlists['items'].extend(load(tests)['items'])
        for playlist in playlists['items']:
            if playlist['id'].startswith("TEST_"):
                with open(self.BASE_DIR + '/../bytepen/test-json/playlists/{}.json'.format(playlist['id'])) as tests:
                    self.write_json("playlists/{}".format(playlist['id']), load(tests))
            else:
                self.write_json("playlists/{}".format(playlist['id']), self.grab_videos_in_playlist(playlist['id']))
        return playlists

    def grab_latest_video(self):
        fields = "items(snippet(title,publishedAt,description,thumbnails/medium),id/videoId)"
        return self.query(self.build_url("search", fields, "date", "", "video", "1"))

    def grab_videos_in_playlist(self, playlist_id: str):
        fields = "items(snippet(title,position,resourceId/videoId,thumbnails/medium))"
        return self.query(self.build_url("playlistItems", fields, "", playlist_id, "playlistItem"))

    def grab_all_videos(self):
        fields = "nextPageToken,items(id(videoId),snippet(title,description))"
        url = self.build_url("search", fields, "date", "", "video")
        videos = self.query(url)
        items = videos['items']
        try:
            next_page = videos["nextPageToken"] if videos["nextPageToken"] else None
            while next_page:
                videos = self.query(url + "&pageToken={}".format(next_page))
                items += videos['items']
                next_page = videos["nextPageToken"] if videos["nextPageToken"] else None
        finally:
            for index, video in enumerate(items):
                items[index]['details'] = self.grab_video_details(video['id']['videoId'])
            return {"items": items}

    def grab_video_details(self, video_id: str):
        fields = "items(snippet(description),statistics(viewCount))"
        result = self.query(self.build_url("videos", fields, "", "", "", "", "snippet,statistics") + "&id=" + video_id)
        return {**result['items'][0]['statistics'], **result['items'][0]['snippet']}
