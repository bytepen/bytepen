from re import findall
from os import listdir, mkdir
from os.path import isdir, isfile
from json import dump


class ArticleBuilder:
    def __init__(self, json_dir: str):
        self.ARTICLES_DIR = json_dir + "/articles"

    @staticmethod
    def parse(file: str) -> {}:
        ret_dic = dict()
        ret_dic['slides'] = []
        with open(file) as fp:
            contents = fp.read().split('-' * 10)[0]
            for i, entry in enumerate(contents.split('-' * 5)):
                urls = findall(r"!\[slide\]\((.*)\)", entry)
                for u in urls:
                    entry = entry.replace("![slide]({})".format(u), "")
                entry = entry.strip().rstrip().replace("\n\n", "<br/>")
                split_content = entry.split("<br/>")
                ret_content = []
                skip_to = 0
                for index, line in enumerate(split_content):
                    if index < skip_to or line == "":
                        continue
                    elif line.startswith('```') and not line.endswith('```'):
                        skip_to = index
                        while not line.endswith('```'):
                            skip_to += 1
                            line += "\n\n"
                            line += split_content[skip_to]

                        skip_to += 1
                    ret_content.append(line)

                ret_dic['slides'].append({"images": urls, "content": ret_content})
        return ret_dic

    def process_files(self, folder: str):
        for file in listdir(folder):
            current = folder + "/" + file
            if isdir(current):
                self.process_files(current)
            elif isfile(current):
                if not isdir(self.ARTICLES_DIR):
                    mkdir(self.ARTICLES_DIR)
                try:
                    with open(current) as fp:
                        video_id = findall(r"VideoID:([A-Za-z0-9-_]*)", fp.read().split('-' * 10)[1])[0]
                    with open(self.ARTICLES_DIR + "/{}.json".format(video_id), 'w') as fp:
                        dump(self.parse(current), fp)
                except IndexError:
                    print("No Video ID: {}".format(current.split("articles/")[1]))
        return

