#!/bin/bash

set -e

main() {
    setup
    buildArticles
}

function cleanup {
    # Windows
    if [ -d "/c/Windows/System32" ]; then
        # Don't close git bash
        if [ "${CURRENT_ENV}" != "CI/CD" ]; then
            echo "[PRESS ENTER TO CONTINUE]"
            read -r trash
        fi
    fi
}
trap cleanup EXIT

setup() {
    BASE_DIR="$(dirname $( cd "$(dirname "$0")" ; pwd -P ))"
    JSON_DIR=${BASE_DIR}/bytepen/src/json
    rm -rf "${JSON_DIR:?}"/* || true

    mkdir -p "${JSON_DIR}"
    mkdir -p "${JSON_DIR}"/playlists
}

function showBanner {
    echo "=================================================================="
    echo "  ${1}"
    echo "=================================================================="
}

function buildArticles {
    showBanner "Building Article JSONs for ${CI_COMMIT_REF_NAME}"
    if [[ -d "/c/Windows/System32" ]]; then
        python "${BASE_DIR}"/scripts/buildJSONs.py
    elif [[ ${CI_COMMIT_REF_NAME} == "master" ]]; then
        python3 "${BASE_DIR}"/scripts/buildJSONs.py --prod
    else
        python3 "${BASE_DIR}"/scripts/buildJSONs.py
    fi
}

main